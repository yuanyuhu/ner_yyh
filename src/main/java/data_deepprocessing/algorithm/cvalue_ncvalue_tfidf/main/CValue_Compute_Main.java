package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.bean.CValueBean;
import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.db.CValue_NCValue_DB;
import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util.CValueTool;
import uk.ac.shef.dcs.oak.jate.JATEException;

/**
 * @author YUANYUHU
 *  
 *  经过修改以后，现在就能够轻易的计算C-Value,现在要做的事情就是把Bootstrapping和C-Value结合起来
 */
public class CValue_Compute_Main {
	
	public static final String output_path = "D:\\cvlaue_test";
	public static final String tablename_SYM_SEED_RULE = "SYM_SEED_RULE";
	public static final String CONNNAME = "yyh_data_new";
	
	public List<CValueBean> getCValueBean(){
		return CValue_NCValue_DB.getSeedInfo2CValue(tablename_SYM_SEED_RULE, CONNNAME);
	}
	
	public List<CValueBean> testCvalue(){
		List<CValueBean> list = new ArrayList<>();
		CValueBean bean1 = new CValueBean();
		bean1.setSeed_Content("口干乏力");
		bean1.setSeed_freqcount(1);
		bean1.setSeed_id(0);
		CValueBean bean2 = new CValueBean();
		bean2.setSeed_Content("口干多饮");
		bean2.setSeed_freqcount(3);
		bean2.setSeed_id(1);
		CValueBean bean3 = new CValueBean();
		bean3.setSeed_Content("口干");
		bean3.setSeed_freqcount(6);
		bean3.setSeed_id(2);
		CValueBean bean4 = new CValueBean();
		bean4.setSeed_Content("多饮");
		bean4.setSeed_freqcount(4);
		bean4.setSeed_id(3);
		CValueBean bean5 = new CValueBean();
		bean5.setSeed_Content("乏力");
		bean5.setSeed_freqcount(3);
		bean5.setSeed_id(4);
		CValueBean bean6 = new CValueBean();
		bean6.setSeed_Content("头晕");
		bean6.setSeed_freqcount(2);
		bean6.setSeed_id(5);
		CValueBean bean7 = new CValueBean();
		bean7.setSeed_Content("手足麻木");
		bean7.setSeed_freqcount(1);
		bean7.setSeed_id(6);
		
		
		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
		list.add(bean4);
		list.add(bean5);
		list.add(bean6);
		list.add(bean7);
		
		return list;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		CValue_Compute_Main cvalue_compute = new CValue_Compute_Main();
		CValueTool cValueTool = new CValueTool(cvalue_compute.testCvalue());
		
		try {
			cValueTool.printCValueResult(cValueTool.CVAlgorithmInvoking(), output_path);
		} catch (JATEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long end = System.currentTimeMillis();
		
		System.out.println("计算消耗的时间为："+(end-start)/1000+"s");
				
	}

}




































