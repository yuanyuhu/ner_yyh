package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util.NCValueTool;
import uk.ac.shef.dcs.oak.jate.JATEException;


public class NCValue_Compute_Main {
	
	public static final String output_path = "D:\\Experiment\\result\\CValue_NCValue_Result";
	
	public static List<String> getSentences(){
		List<String> sentences = new ArrayList<>();
		String sentence1 = "间断口干乏力2年，加重伴头晕2月。";
		String sentence2 = "间断口干多饮7年，加重伴乏力2月。";
		String sentence3 = "口干口渴伴尿中泡沫1月余，加重1周。";
		String sentence4 = "间断口干、多饮、乏力17年，加重伴头晕目眩1月。";
		String sentence5 = "间断口干多饮22年，加重伴手足麻木疼痛1月。";
		String sentence6 = "间断口干多饮16年，加重2月。";
		
		sentences.add(sentence1);
		sentences.add(sentence2);
		sentences.add(sentence3);
		sentences.add(sentence4);
		sentences.add(sentence5);
		sentences.add(sentence6);
		
		return sentences; 
	}
	public static List<String> getOrderedSentences(){
		List<String> sentences = new ArrayList<>();
		String sentence1 = "间断口干乏力TUS加重伴头晕TUS";
		String sentence2 = "间断口干多饮TUS加重伴乏力TUS";
		String sentence3 = "口干口渴伴尿中泡沫TUS加重TUS";
		String sentence4 = "间断口干S多饮S乏力TUS加重伴头晕目眩TUS";
		String sentence5 = "间断口干多饮TUS加重伴手足麻木疼痛TUS";
		String sentence6 = "间断口干多饮TUS加重TUS";
		
		sentences.add(sentence1);
		sentences.add(sentence2);
		sentences.add(sentence3);
		sentences.add(sentence4);
		sentences.add(sentence5);
		sentences.add(sentence6);
		
		return sentences; 
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NCValueTool tool = new NCValueTool();
		
		try {
			tool.NCVAlgorithmInvoking(output_path, NCValue_Compute_Main.getOrderedSentences());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JATEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
