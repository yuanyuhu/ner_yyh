package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.bean;

/**
 * @author YUANYUHU
 *
 */
public class CValueBean  {
	
	private String seed_Content;
	private Integer seed_id;
	private Integer seed_freqcount;
	private String chunk_name;
	
	
	@Override
	public String toString() {
		return "CValueBean [seed_Content=" + seed_Content + ", seed_id=" + seed_id + ", seed_freqcount="
				+ seed_freqcount + ", chunk_name=" + chunk_name + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chunk_name == null) ? 0 : chunk_name.hashCode());
		result = prime * result + ((seed_Content == null) ? 0 : seed_Content.hashCode());
		result = prime * result + ((seed_freqcount == null) ? 0 : seed_freqcount.hashCode());
		result = prime * result + ((seed_id == null) ? 0 : seed_id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CValueBean other = (CValueBean) obj;
		if (chunk_name == null) {
			if (other.chunk_name != null)
				return false;
		} else if (!chunk_name.equals(other.chunk_name))
			return false;
		if (seed_Content == null) {
			if (other.seed_Content != null)
				return false;
		} else if (!seed_Content.equals(other.seed_Content))
			return false;
		if (seed_freqcount == null) {
			if (other.seed_freqcount != null)
				return false;
		} else if (!seed_freqcount.equals(other.seed_freqcount))
			return false;
		if (seed_id == null) {
			if (other.seed_id != null)
				return false;
		} else if (!seed_id.equals(other.seed_id))
			return false;
		return true;
	}
	
	
	
	public String getSeed_Content() {
		return seed_Content;
	}
	public void setSeed_Content(String seed_Content) {
		this.seed_Content = seed_Content;
	}
	public Integer getSeed_id() {
		return seed_id;
	}
	public void setSeed_id(Integer seed_id) {
		this.seed_id = seed_id;
	}
	public Integer getSeed_freqcount() {
		return seed_freqcount;
	}
	public void setSeed_freqcount(Integer seed_freqcount) {
		this.seed_freqcount = seed_freqcount;
	}
	public String getChunk_name() {
		return chunk_name;
	}
	public void setChunk_name(String chunk_name) {
		this.chunk_name = chunk_name;
	}
	
}
