package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util;

import java.util.HashMap;

import uk.ac.shef.dcs.oak.jate.core.feature.indexer.GlobalIndexMem;

/**
 * @author YUANYUHU
 *
 */
public class GlobalIndexYYH extends GlobalIndexMem{
	
	protected void setTermIdMap(HashMap<String, Integer> _termIdMap) {
		super._termIdMap = _termIdMap;
	}
	
	@Override
	public String retrieveTermCanonical(int id) {
        return CValueTool.index_term_map.get(id);
	}
	
}
