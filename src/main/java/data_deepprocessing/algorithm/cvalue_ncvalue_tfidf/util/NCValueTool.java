package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.bean.CValueBean;
import data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.db.CValue_NCValue_DB;
import uk.ac.shef.dcs.oak.jate.JATEException;
import uk.ac.shef.dcs.oak.jate.core.algorithm.NCValueFeatureWrapper;
import uk.ac.shef.dcs.oak.jate.test.AlgorithmTester;

public class NCValueTool {
	
	private List<CValueBean> cValueBeans;
	
	public NCValueTool(String tableName,String connName){
		this.cValueBeans = CValue_NCValue_DB.getSeedInfo2CValue(tableName, connName);
	}
	
	public NCValueTool(){
		this.cValueBeans = testCvalue();
	}
	
	public static List<CValueBean> testCvalue(){
		List<CValueBean> list = new ArrayList<>();
		CValueBean bean1 = new CValueBean();
		bean1.setSeed_Content("口干乏力");
		bean1.setSeed_freqcount(1);
		bean1.setSeed_id(0);
		CValueBean bean2 = new CValueBean();
		bean2.setSeed_Content("口干多饮");
		bean2.setSeed_freqcount(3);
		bean2.setSeed_id(1);
		CValueBean bean3 = new CValueBean();
		bean3.setSeed_Content("口干");
		bean3.setSeed_freqcount(6);
		bean3.setSeed_id(2);
		CValueBean bean4 = new CValueBean();
		bean4.setSeed_Content("多饮");
		bean4.setSeed_freqcount(4);
		bean4.setSeed_id(3);
		CValueBean bean5 = new CValueBean();
		bean5.setSeed_Content("乏力");
		bean5.setSeed_freqcount(3);
		bean5.setSeed_id(4);
		CValueBean bean6 = new CValueBean();
		bean6.setSeed_Content("头晕");
		bean6.setSeed_freqcount(2);
		bean6.setSeed_id(5);
		CValueBean bean7 = new CValueBean();
		bean7.setSeed_Content("手足麻木");
		bean7.setSeed_freqcount(1);
		bean7.setSeed_id(6);
		
		
		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
		list.add(bean4);
		list.add(bean5);
		list.add(bean6);
		list.add(bean7);
		
		return list;
	}
	
	/**
	 * 得到C-Value的算法
	 * @return
	 * @throws IOException 
	 * @throws JATEException 
	 */
	public AlgorithmTester getCValueAlgorithmTester() throws JATEException, IOException{
		
		CValueTool cValueTool = new CValueTool(cValueBeans);
		
		AlgorithmTester algorithmTester = cValueTool.CVAlgorithmInvoking();
		
		return algorithmTester;
	}
	
	/**
	 * @param output_path
	 * @return AlgorithmTester  返回一个值为了给NC-Value使用
	 * @throws IOException 
	 * @throws JATEException 
	 */
	public void NCVAlgorithmInvoking(String output, List<String> sentences) throws IOException, JATEException{
		
 		System.out.println("Started "+ NCValueTool.class+" at: " + new Date() + "... For detailed progress see log file jate.log.");
		//得到CValue的计算信息
		AlgorithmTester tester = getCValueAlgorithmTester();
		//我自己的停用词表，这部分的内容还是要下一定的工夫的
		StopListYYH stoplist = new StopListYYH();
		//抽取上下文信息context
		ContextExtractionYYH contextExtract = new ContextExtractionYYH(tester,stoplist);
		
		Map<String, Double> contextWords = contextExtract.Extract(sentences);
		//组装算法准备计算相应的算法
		AlgorithmTester NCTester = new AlgorithmTester();
		
		NCTester.registerAlgorithm(new NCValueAlgorithmYYH(contextExtract,stoplist), new NCValueFeatureWrapper(contextWords, tester));
		
		NCTester.execute(null, output);
		
		System.out.println("Ended at: " + new Date());	
	}
	
	
	
	

}

































