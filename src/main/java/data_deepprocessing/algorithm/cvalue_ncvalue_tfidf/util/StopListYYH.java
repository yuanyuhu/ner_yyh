package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import uk.ac.shef.dcs.oak.jate.util.control.StopList;

/**
 * @author YuanYuhu stoplist.txt现在要使用起来了
 */
public class StopListYYH extends StopList {

	private static final long serialVersionUID = 1L;

	/**
	 * @throws IOException
	 *             直接加载停用词表
	 */
	public StopListYYH() throws IOException {
		String stoplist_path = StopListYYH.class.getClassLoader().getResource("stoplist.txt").getPath();
		loadStopList(new File(stoplist_path));
	}

	/**
	 * @param word
	 * @return true if the word is a stop word, false if otherwise 中文不需要大小写敏感
	 */
	public boolean isStopWord(String word) {
		return contains(word);
	}

	private void loadStopList(final File stopListFile) throws IOException {
		final BufferedReader reader = new BufferedReader(new FileReader(stopListFile));
		String line;
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			if (line.equals("") || line.startsWith("//"))
				continue;
			this.add(line);
		}
		if (reader != null) {
			reader.close();

		}
	}
	
	public static void main(String[] args) throws IOException{
		StopListYYH stopListYYH = new StopListYYH();
		System.out.println(stopListYYH.isStopWord("yuan"));
		
	}

}











