package data_deepprocessing.algorithm.cvalue_ncvalue_tfidf.util;

import java.util.HashSet;
import java.util.Set;

public class UtilityYYH {
	
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年3月9日 下午7:45:11 
	* @parameter 
	* @return set<String> word is exist in sentence
	* @throws 
	* 把句子中存在的所有词都给拿出来，
	*/
	public static Set<String> getTermVariants_sent(String sentence, Set<String> variants){
		Set<String> TermVariants_Sent = new HashSet<String>();	
		for(String variant: variants) {
			if(sentence.contains(variant)) {
				TermVariants_Sent.add(variant);			
			}
		}
		return TermVariants_Sent;
	}

}
