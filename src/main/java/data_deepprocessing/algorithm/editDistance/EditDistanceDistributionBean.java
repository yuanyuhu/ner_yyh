package data_deepprocessing.algorithm.editDistance;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2016年11月25日 下午4:15:57 
* @version 1.0  
*/

public class EditDistanceDistributionBean {
	
	private int section;
	private int count;
	private double normalization;
	
	
	public int getSection() {
		return section;
	}
	public void setSection(int section) {
		this.section = section;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getNormalization() {
		return normalization;
	}
	public void setNormalization(double normalization) {
		this.normalization = normalization;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		long temp;
		temp = Double.doubleToLongBits(normalization);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + section;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EditDistanceDistributionBean other = (EditDistanceDistributionBean) obj;
		if (count != other.count)
			return false;
		if (Double.doubleToLongBits(normalization) != Double.doubleToLongBits(other.normalization))
			return false;
		if (section != other.section)
			return false;
		return true;
	}
	
	
	
	
}
