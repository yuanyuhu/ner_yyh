package data_deepprocessing.algorithm.editDistance;
//package data_deepprocessing.algorithm.editDistance;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//
//public class EditDistance {
//
//	Map<Integer, Integer> section_count_map = new HashMap<>();
//	List<EditDistanceBean> distanceList = new ArrayList<>();
//	List<EditDistanceDistributionBean> distributionList = new ArrayList<>();
//
//	private static int compute(String s1, String s2) {
//		int[][] dp = new int[s1.length() + 1][s2.length() + 1];
//		for (int i = 0; i < dp.length; i++) {
//			for (int j = 0; j < dp[i].length; j++) {
//				dp[i][j] = i == 0 ? j : j == 0 ? i : 0;
//				if (i > 0 && j > 0) {
//					if (s1.charAt(i - 1) == s2.charAt(j - 1))
//						dp[i][j] = dp[i - 1][j - 1];
//					else
//						dp[i][j] = Math.min(dp[i][j - 1] + 1, Math.min(dp[i - 1][j - 1] + 1, dp[i - 1][j] + 1));
//				}
//			}
//		}
//		return dp[s1.length()][s2.length()];
//	}
//
//	
//
//	/**
//	 * @param list
//	 *            计算所有的编辑距离分布
//	 */
//	private void getEditDistanceDistributionData(List<XianBingShi_Content_Bean> list) {
//		for (int i = 0; i < list.size() - 1; ++i) {
//			for (int j = i + 1; j < list.size(); ++j) {
//				XianBingShi_Content_Bean i_bean = list.get(i);
//				XianBingShi_Content_Bean j_bean = list.get(j);
//
//				int distance = EditDistance.compute(i_bean.getXianbingshi(), j_bean.getXianbingshi());
//				int i_bean_length = i_bean.getXianbingshi().length();
//				int j_bean_length = j_bean.getXianbingshi().length();
//				int length = i_bean_length > j_bean_length ? i_bean_length : j_bean_length;
//
//				EditDistanceBean bean = new EditDistanceBean();
//				bean.setId1(i_bean.getZhang_id());
//				bean.setId2(j_bean.getZhang_id());
//				double distanceDouble = (distance * 1.0)/length;
//				bean.setDistance(distanceDouble);
//				getDistanceDistributioinMap(distanceDouble);
//				distanceList.add(bean);
//			}
//		}
//	}
//
//	/**
//	 * @param list
//	 *            计算所有的编辑距离分布
//	 */
//	private void getDistanceDistributioinMap(double distance) {
//		int condition = 0;
//		if (distance >= 0 && distance < 0.1) {
//			condition = 1;
//		} else if (distance >= 0.1 && distance < 0.2) {
//			condition = 2;
//		} else if (distance >= 0.2 && distance < 0.3) {
//			condition = 3;
//		} else if (distance >= 0.3 && distance < 0.4) {
//			condition = 4;
//		} else if (distance >= 0.4 && distance < 0.5) {
//			condition = 5;
//		} else if (distance >= 0.5 && distance < 0.6) {
//			condition = 6;
//		} else if (distance >= 0.6 && distance < 0.7) {
//			condition = 7;
//		} else if (distance >= 0.7 && distance < 0.8) {
//			condition = 8;
//		} else if (distance >= 0.8 && distance < 0.9) {
//			condition = 9;
//		} else if (distance >= 0.9 && distance < 1) {
//			condition = 10;
//		} else if (distance == 1) {
//			condition = 11;
//		}
//
//		switch (condition) {
//		case 1:
//			if (section_count_map.containsKey(1)) {
//				section_count_map.put(1, section_count_map.get(1) + 1);
//			} else {
//				section_count_map.put(1, 1);
//			}
//			break;
//		case 2:
//			if (section_count_map.containsKey(2))
//				section_count_map.put(2, section_count_map.get(2) + 1);
//			else
//				section_count_map.put(2, 1);
//			break;
//		case 3:
//			if (section_count_map.containsKey(3))
//				section_count_map.put(3, section_count_map.get(3) + 1);
//			else
//				section_count_map.put(3, 1);
//			break;
//		case 4:
//			if (section_count_map.containsKey(4))
//				section_count_map.put(4, section_count_map.get(4) + 1);
//			else
//				section_count_map.put(4, 1);
//			break;
//		case 5:
//			if (section_count_map.containsKey(5))
//				section_count_map.put(5, section_count_map.get(5) + 1);
//			else
//				section_count_map.put(5, 1);
//			break;
//		case 6:
//			if (section_count_map.containsKey(6))
//				section_count_map.put(6, section_count_map.get(6) + 1);
//			else
//				section_count_map.put(6, 1);
//			break;
//		case 7:
//			if (section_count_map.containsKey(7))
//				section_count_map.put(7, section_count_map.get(7) + 1);
//			else
//				section_count_map.put(7, 1);
//			break;
//		case 8:
//			if (section_count_map.containsKey(8))
//				section_count_map.put(8, section_count_map.get(8) + 1);
//			else
//				section_count_map.put(8, 1);
//			break;
//		case 9:
//			if (section_count_map.containsKey(9))
//				section_count_map.put(9, section_count_map.get(9) + 1);
//			else
//				section_count_map.put(9, 1);
//			break;
//		case 10:
//			if (section_count_map.containsKey(10))
//				section_count_map.put(10, section_count_map.get(10) + 1);
//			else
//				section_count_map.put(10, 1);
//
//			break;
//		case 11:
//			if (section_count_map.containsKey(11))
//				section_count_map.put(11, section_count_map.get(11) + 1);
//			else
//				section_count_map.put(11, 1);
//
//			break;
//		default:
//			break;
//		}
//
//	}
//	
//	private void  getDistanceDistributeResult(){
//		for(int section : section_count_map.keySet()){
//			EditDistanceDistributionBean bean = new EditDistanceDistributionBean();
//			bean.setSection(section);
//			int count = section_count_map.get(section);
//			bean.setCount(count);
//			bean.setNormalization(count*1.0/distanceList.size());
//			distributionList.add(bean);
//		}
//		if(!section_count_map.isEmpty()){
//			section_count_map.clear();
//		}
//	}
//	
//
//
//
//	private void insertEditDistanceSet(String tag) {
//		try {
//			EditDistance_DB.insertEditDistanceSet(distanceList,tag);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (!distanceList.isEmpty()) {
//			distanceList.clear();
//		}
//	}
//
//
//	
//	private void insertEditDistributionSet(String tag){
//		try {
//			EditDistance_DB.insertEditDistributionSet(distributionList,tag);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (!distributionList.isEmpty()) {
//			distributionList.clear();
//		}
//	}
//
//	
//	/**
//	 * 形成编辑距离的分布和编辑距离
//	 */
//	public static void main(String[] args){
//		 EditDistance editDistance = new EditDistance();
//		
//		 try {
//		 List<XianBingShi_Content_Bean> yuanList = EditDistance_DB.getYuanDataSet();
//		 List<XianBingShi_Content_Bean> zhangList = EditDistance_DB.getZhangDataSet();
//		 List<XianBingShi_Content_Bean> hybridList = EditDistance_DB.getHybridDataSet();
//		 editDistance.getEditDistanceDistributionData(yuanList);
//		 editDistance.getDistanceDistributeResult();
//		 editDistance.insertEditDistanceSet("yuan");
//		 editDistance.insertEditDistributionSet("yuan");
//		 
//		 editDistance.getEditDistanceDistributionData(zhangList);
//		 editDistance.getDistanceDistributeResult();
//		 editDistance.insertEditDistanceSet("zhang");
//		 editDistance.insertEditDistributionSet("zhang");
//		 
//		 editDistance.getEditDistanceDistributionData(hybridList);
//		 editDistance.getDistanceDistributeResult();
//		 editDistance.insertEditDistanceSet("hybrid");
//		 editDistance.insertEditDistributionSet("hybrid");
//		 
//		 
//		 } catch (Exception e) {
//		 // TODO Auto-generated catch block
//		 e.printStackTrace();
//		 }
//		
//	}
//	
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
