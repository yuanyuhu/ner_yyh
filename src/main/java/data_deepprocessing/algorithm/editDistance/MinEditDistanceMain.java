package data_deepprocessing.algorithm.editDistance;
//package data_deepprocessing.algorithm.editDistance;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//
///** 
//* @author  作者 : YUHU YUAN
//* @date 创建时间：2016年12月1日 下午10:18:57 
//* @version 1.0  
//*/
//
//public class MinEditDistance {
//	
//	List<MinDistanceBean> minDistanceBeans = new ArrayList<>();
//	Map<Integer, Integer> section_count_map = new HashMap<>();
//	List<EditDistanceDistributionBean> distributionList = new ArrayList<>();
//	
//	public void getMinDistance(String tag, List<Integer> idList) throws IOException{
//		for(int id: idList){
//			MinDistanceBean bean = EditDistance_DB.getHerbInfoForStatistics(tag, id);
//			getDistanceDistributioinMap(bean.getMinDistance());
//			minDistanceBeans.add(bean);
//		}
//	}
//	
//	/**
//	 * @param list
//	 *            计算所有的编辑距离分布
//	 */
//	private void getDistanceDistributioinMap(double distance) {
//		int condition = 0;
//		if (distance >= 0 && distance < 0.1) {
//			condition = 1;
//		} else if (distance >= 0.1 && distance < 0.2) {
//			condition = 2;
//		} else if (distance >= 0.2 && distance < 0.3) {
//			condition = 3;
//		} else if (distance >= 0.3 && distance < 0.4) {
//			condition = 4;
//		} else if (distance >= 0.4 && distance < 0.5) {
//			condition = 5;
//		} else if (distance >= 0.5 && distance < 0.6) {
//			condition = 6;
//		} else if (distance >= 0.6 && distance < 0.7) {
//			condition = 7;
//		} else if (distance >= 0.7 && distance < 0.8) {
//			condition = 8;
//		} else if (distance >= 0.8 && distance < 0.9) {
//			condition = 9;
//		} else if (distance >= 0.9 && distance < 1) {
//			condition = 10;
//		} else if (distance == 1) {
//			condition = 11;
//		}
//
//		switch (condition) {
//		case 1:
//			if (section_count_map.containsKey(1)) {
//				section_count_map.put(1, section_count_map.get(1) + 1);
//			} else {
//				section_count_map.put(1, 1);
//			}
//			break;
//		case 2:
//			if (section_count_map.containsKey(2))
//				section_count_map.put(2, section_count_map.get(2) + 1);
//			else
//				section_count_map.put(2, 1);
//			break;
//		case 3:
//			if (section_count_map.containsKey(3))
//				section_count_map.put(3, section_count_map.get(3) + 1);
//			else
//				section_count_map.put(3, 1);
//			break;
//		case 4:
//			if (section_count_map.containsKey(4))
//				section_count_map.put(4, section_count_map.get(4) + 1);
//			else
//				section_count_map.put(4, 1);
//			break;
//		case 5:
//			if (section_count_map.containsKey(5))
//				section_count_map.put(5, section_count_map.get(5) + 1);
//			else
//				section_count_map.put(5, 1);
//			break;
//		case 6:
//			if (section_count_map.containsKey(6))
//				section_count_map.put(6, section_count_map.get(6) + 1);
//			else
//				section_count_map.put(6, 1);
//			break;
//		case 7:
//			if (section_count_map.containsKey(7))
//				section_count_map.put(7, section_count_map.get(7) + 1);
//			else
//				section_count_map.put(7, 1);
//			break;
//		case 8:
//			if (section_count_map.containsKey(8))
//				section_count_map.put(8, section_count_map.get(8) + 1);
//			else
//				section_count_map.put(8, 1);
//			break;
//		case 9:
//			if (section_count_map.containsKey(9))
//				section_count_map.put(9, section_count_map.get(9) + 1);
//			else
//				section_count_map.put(9, 1);
//			break;
//		case 10:
//			if (section_count_map.containsKey(10))
//				section_count_map.put(10, section_count_map.get(10) + 1);
//			else
//				section_count_map.put(10, 1);
//
//			break;
//		case 11:
//			if (section_count_map.containsKey(11))
//				section_count_map.put(11, section_count_map.get(11) + 1);
//			else
//				section_count_map.put(11, 1);
//
//			break;
//		default:
//			break;
//		}
//
//	}
//	
//	private void insertMinEditDistanceSet(String tag) {
//		try {
//			EditDistance_DB.insertMinEditDistanceSet(minDistanceBeans,tag);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (!minDistanceBeans.isEmpty()) {
//			minDistanceBeans.clear();
//		}
//	}
//	
//	
//	private void insertMinEditDistributionSet(String tag){
//		try {
//			EditDistance_DB.insertMinEditDistributionSet(distributionList,tag);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (!distributionList.isEmpty()) {
//			distributionList.clear();
//		}
//	}
//	
//	private void  getDistanceDistributeResult(){
//		for(int section : section_count_map.keySet()){
//			EditDistanceDistributionBean bean = new EditDistanceDistributionBean();
//			bean.setSection(section);
//			int count = section_count_map.get(section);
//			bean.setCount(count);
//			bean.setNormalization(count*1.0/minDistanceBeans.size());
//			distributionList.add(bean);
//		}
//		if(!section_count_map.isEmpty()){
//			section_count_map.clear();
//		}
//	}
//	
//	public static void main(String[] args) {
//		MinEditDistance minEditDistance = new MinEditDistance();
//		try {
//			List<Integer> yuan_ids = EditDistance_DB.getYuan_IDs();
//			List<Integer> zhang_ids = EditDistance_DB.getZhang_IDs();
//			List<Integer> hybrid_ids = EditDistance_DB.getHybrid_IDs();
//
//			minEditDistance.getMinDistance("yuan", yuan_ids);
//			minEditDistance.getDistanceDistributeResult();
//			minEditDistance.insertMinEditDistanceSet("yuan");
//			minEditDistance.insertMinEditDistributionSet("yuan");
//			
//			minEditDistance.getMinDistance("zhang", zhang_ids);
//			minEditDistance.getDistanceDistributeResult();
//			minEditDistance.insertMinEditDistanceSet("zhang");
//			minEditDistance.insertMinEditDistributionSet("zhang");
//			
//			minEditDistance.getMinDistance("hybrid", hybrid_ids);
//			minEditDistance.getDistanceDistributeResult();
//			minEditDistance.insertMinEditDistanceSet("hybrid");
//			minEditDistance.insertMinEditDistributionSet("hybrid");
//			 
//			 
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//	}
//
//}
