package data_deepprocessing.algorithm.editDistance;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2016年12月1日 下午10:43:49 
* @version 1.0  
*/

public class MinDistanceBean {
	
	private int id;
	
	private double minDistance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getMinDistance() {
		return minDistance;
	}

	public void setMinDistance(double minDistance) {
		this.minDistance = minDistance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(minDistance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinDistanceBean other = (MinDistanceBean) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(minDistance) != Double.doubleToLongBits(other.minDistance))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MinDistanceBean [id=" + id + ", minDistance=" + minDistance + "]";
	}
	
	
	
	

}
