//package data_deepprocessing.algorithm.editDistance;
//
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//import edu.stanford.nlp.io.EncodingPrintWriter;
//import temp_function_crfs_bean.XianBingShi_Content_Bean;
//import temp_function_db_util.DbUtil;
//
///** 
//* @author  作者 : YUHU YUAN
//* @date 创建时间：2016年11月25日 上午11:24:27 
//* @version 1.0  
//*/
//
//public class EditDistance_DB {
//
//	
//	public static List<XianBingShi_Content_Bean> getZhangDataSet() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<XianBingShi_Content_Bean> list=new ArrayList<XianBingShi_Content_Bean>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.zhang_id,t.subxianbingshi from ZHANG_DIVISIONXBS_CHULIED t where t.tag is not  null");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				XianBingShi_Content_Bean xianbingshiInfo = new XianBingShi_Content_Bean();
//				xianbingshiInfo.setZhang_id(rs.getInt("zhang_id"));
//				xianbingshiInfo.setXianbingshi(rs.getString("subxianbingshi"));
//				list.add(xianbingshiInfo);
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	public static List<Integer> getZhang_IDs() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<Integer> list=new ArrayList<Integer>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.zhang_id from ZHANG_DIVISIONXBS_CHULIED t where t.tag is not  null");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				list.add(rs.getInt("zhang_id"));
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	
//	public static List<XianBingShi_Content_Bean> getYuanDataSet() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<XianBingShi_Content_Bean> list=new ArrayList<XianBingShi_Content_Bean>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.yuan_id,t.subxianbingshi from YUAN_DIVISIONXBS_CHULIED t where t.tag is not null");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				XianBingShi_Content_Bean xianbingshiInfo = new XianBingShi_Content_Bean();
//				xianbingshiInfo.setZhang_id(rs.getInt("yuan_id"));
//				xianbingshiInfo.setXianbingshi(rs.getString("subxianbingshi"));
//				list.add(xianbingshiInfo);
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	public static List<Integer> getYuan_IDs() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<Integer> list=new ArrayList<Integer>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.yuan_id from YUAN_DIVISIONXBS_CHULIED t where t.tag is not  null");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				list.add(rs.getInt("yuan_id"));
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	public static List<XianBingShi_Content_Bean> getHybridDataSet() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<XianBingShi_Content_Bean> list=new ArrayList<XianBingShi_Content_Bean>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.zhang_id as hybrid_id,t.subxianbingshi as hybrid_subxianbingshi from ZHANG_DIVISIONXBS_CHULIED t where t.tag is not  null "
//						+"union	"
//						+"select t.yuan_id,t.subxianbingshi from YUAN_DIVISIONXBS_CHULIED t where t.tag is not null	");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				XianBingShi_Content_Bean xianbingshiInfo = new XianBingShi_Content_Bean();
//				xianbingshiInfo.setZhang_id(rs.getInt("hybrid_id"));
//				xianbingshiInfo.setXianbingshi(rs.getString("hybrid_subxianbingshi"));
//				list.add(xianbingshiInfo);
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	
//	public static List<Integer> getHybrid_IDs() throws Exception {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		List<Integer> list=new ArrayList<Integer>();
//		try {
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select t.zhang_id as hybrid_id  from ZHANG_DIVISIONXBS_CHULIED t where t.tag is not  null "
//					+"union	"
//					+"select t.yuan_id from YUAN_DIVISIONXBS_CHULIED t where t.tag is not null	");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				list.add(rs.getInt("hybrid_id"));
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return list;
//	}
//	
//	
//	
//	public static void  insertEditDistanceSet(List<EditDistanceBean> distanceList, String tag) throws SQLException, IOException{
//		Connection conn = DbUtil.getInstance().getConnection("yyh_data_new");
//		PreparedStatement Pstatement = null;
//		boolean autoCommit = conn.getAutoCommit();
//		conn.setAutoCommit(false);
//		String sql = "insert into XBS_EDIT_DISTANCE VALUES(?,?,?,?)";
//		Pstatement = conn.prepareStatement(sql);
//		final int batchSize = 1000;
//		int count=0;
//		for(EditDistanceBean bean: distanceList){
//			Pstatement.setInt(1, bean.getId1());
//			Pstatement.setInt(2, bean.getId2());
//			Pstatement.setDouble(3, bean.getDistance());
//			Pstatement.setString(4, tag);
//			Pstatement.addBatch();
//			++count;
//			if(count%batchSize == 0){
//				System.out.println(count);
//				try {
//					Pstatement.executeBatch();
//					count=0;
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}finally{
//					conn.commit();
//				}
//			}
//		}
//		try{
//			Pstatement.executeBatch();
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			conn.setAutoCommit(autoCommit);
//			DbUtil.getInstance().closeConn(conn,Pstatement);
//		}
//	}
//	
//	public static void  insertMinEditDistanceSet(List<MinDistanceBean> minDistanceList, String tag) throws SQLException, IOException{
//		Connection conn = DbUtil.getInstance().getConnection("yyh_data_new");
//		PreparedStatement Pstatement = null;
//		boolean autoCommit = conn.getAutoCommit();
//		conn.setAutoCommit(false);
//		String sql = "insert into XBS_MIN_EDIT_DISTANCE VALUES(?,?,?)";
//		Pstatement = conn.prepareStatement(sql);
//		final int batchSize = 1000;
//		int count=0;
//		for(MinDistanceBean bean: minDistanceList){
//			Pstatement.setInt(1, bean.getId());
//			Pstatement.setDouble(2, bean.getMinDistance());
//			Pstatement.setString(3, tag);
//			Pstatement.addBatch();
//			++count;
//			if(count%batchSize == 0){
//				System.out.println(count);
//				try {
//					Pstatement.executeBatch();
//					count=0;
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}finally{
//					conn.commit();
//				}
//			}
//		}
//		try{
//			Pstatement.executeBatch();
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			conn.setAutoCommit(autoCommit);
//			DbUtil.getInstance().closeConn(conn,Pstatement);
//		}
//	}
//	
//	
//	
//	public static void  insertEditDistributionSet(List<EditDistanceDistributionBean> distributionList, String tag) throws SQLException, IOException{
//		Connection conn = DbUtil.getInstance().getConnection("yyh_data_new");
//		PreparedStatement Pstatement = null;
//		boolean autoCommit = conn.getAutoCommit();
//		conn.setAutoCommit(false);
//		String sql = "insert into XBS_EDIT_DISTRIBUTION VALUES(?,?,?,?)";
//		Pstatement = conn.prepareStatement(sql);
//		final int batchSize = 1000;
//		int count=0;
//		for(EditDistanceDistributionBean bean: distributionList){
//			Pstatement.setInt(1, bean.getSection());
//			Pstatement.setInt(2, bean.getCount());
//			Pstatement.setDouble(3, bean.getNormalization());
//			Pstatement.setString(4, tag);
//			Pstatement.addBatch();
//			++count;
//			if(count%batchSize == 0){
//				System.out.println(count);
//				try {
//					Pstatement.executeBatch();
//					count=0;
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}finally{
//					conn.commit();
//				}
//			}
//		}
//		try{
//			Pstatement.executeBatch();
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			conn.setAutoCommit(autoCommit);
//			DbUtil.getInstance().closeConn(conn,Pstatement);
//		}
//	}
//	
//	public static void  insertMinEditDistributionSet(List<EditDistanceDistributionBean> distributionList, String tag) throws SQLException, IOException{
//		Connection conn = DbUtil.getInstance().getConnection("yyh_data_new");
//		PreparedStatement Pstatement = null;
//		boolean autoCommit = conn.getAutoCommit();
//		conn.setAutoCommit(false);
//		String sql = "insert into XBS_MIN_EDIT_DISTRIBUTION VALUES(?,?,?,?)";
//		Pstatement = conn.prepareStatement(sql);
//		final int batchSize = 1000;
//		int count=0;
//		for(EditDistanceDistributionBean bean: distributionList){
//			Pstatement.setInt(1, bean.getSection());
//			Pstatement.setInt(2, bean.getCount());
//			Pstatement.setDouble(3, bean.getNormalization());
//			Pstatement.setString(4, tag);
//			Pstatement.addBatch();
//			++count;
//			if(count%batchSize == 0){
//				System.out.println(count);
//				try {
//					Pstatement.executeBatch();
//					count=0;
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}finally{
//					conn.commit();
//				}
//			}
//		}
//		try{
//			Pstatement.executeBatch();
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			conn.setAutoCommit(autoCommit);
//			DbUtil.getInstance().closeConn(conn,Pstatement);
//		}
//	}
//	
//	
//	public static MinDistanceBean getHerbInfoForStatistics(String tag, int id) throws IOException{
//		Connection conn = null;
//		PreparedStatement ps = null;
//		StringBuffer sql = new StringBuffer();
//		ResultSet rs = null;
//		MinDistanceBean bean = new MinDistanceBean();
//		try{
//			conn = DbUtil.getInstance().getConnection("yyh_data_new");
//			sql.append("select  min(t.m) as minV from ");
//			sql.append("(select min(t.distance) as m from XBS_EDIT_DISTANCE t where t.hybrid_id2 = ").append(id).append(" and tag like '").append(tag).append("' ");
//			sql.append("union ");
//			sql.append("select min(t.distance) from XBS_EDIT_DISTANCE t where t.hybrid_id1 = ").append(id).append(" and tag like '").append(tag).append("' ) t ");
//			ps = conn.prepareStatement(sql.toString());
//			rs = ps.executeQuery(sql.toString());
//			while(rs.next()){
//				bean.setId(id);
//				bean.setMinDistance(rs.getDouble("minV"));
//			}
//		}catch(SQLException e){
//			e.printStackTrace();;
//		}finally{
//			DbUtil.getInstance().closeConn(conn, ps, rs);
//		}
//		return bean;
//	}
//	
//	
//	public static void main(String[] args){
//	}
//	
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
