package data_deepprocessing.algorithm.crfs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.util.RegularExpressionUtil;
import data_deepprocessing.util.TxtOperate;


public class CreateTrainDataSet2En {
	
	private String[][] entityArray;
	
	private static List<String> seeds = new ArrayList<>(); 
	
	/**
	 * 想要使用该函数就先调用此方法
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年5月1日 下午9:14:18 
	* @parameter 
	* @return
	* @throws
	 */
	
	public void initEntityArray(List<String> initSeeds){
		for(String content : initSeeds){
			content = RegularExpressionUtil.HandlePunctuation2EnSentence(content);
			if(content!=null && !content.equals("")){
				seeds.add(content);
			}
		}
		
		entityArray = new String[seeds.size()][];
		for(int i=0; i<seeds.size(); ++i){
			String seed = seeds.get(i);
			entityArray[i] = seed.split(" ");
		}
		
		
	}
	
	
	/**
	 * 这里的程序数组越界异常原因：entity有多个单词组成，其中sentence的结尾正好是entity的前面的部分单词
	 * 在这里进行了异常的处理，最终得到的结果是正确的
	 * @param entityList
	 * @param sentence_content
	 * @return
	 */
	private StringBuffer processingTrainDataSet(String sentence_content){
		sentence_content = RegularExpressionUtil.HandlePunctuation2EnSentence(sentence_content);
		String[] sentenceArray = sentence_content.split(" ");
		StringBuffer content = new StringBuffer();
		for(int i = 0; i < sentenceArray.length; ){
			int state = -1 ;
			for(int k = 0; k < seeds.size(); k++){
				try{
					for(int j=0; j<entityArray[k].length; j++){
						if(sentenceArray[i+j].toUpperCase().equals(entityArray[k][j].toUpperCase()))
							;
						else
						{
							state=0;
							break;
						}
						if(j == entityArray[k].length - 1)
							state = k + 1;
					}
					if(state == k + 1)
						break;
				}catch(ArrayIndexOutOfBoundsException e){
					System.out.println("构建英文CRFs数据，产生了异常，忽略该异常；");
					state=0;
				}
			}
			if(state != 0){
				content.append(sentenceArray[i]+"\tB-S").append("\r\n")  ;
				for(int j=1; j<entityArray[state - 1].length; j++)
					content.append(sentenceArray[i+j]+"\tE-S").append("\r\n");
				i += entityArray[state - 1].length;
			}else{
				content.append(sentenceArray[i]+"\tO").append("\r\n");
				i++;
			}
		}
		return content;
	}
	
	
	public void doCreateENCRFData(String filePath,List<String> initSeeds,List<XianBingShi_new_zhangBean> xianbingshiInfos){
		if(entityArray==null || entityArray.length ==0){
			initEntityArray(initSeeds);
		}
		
		for(XianBingShi_new_zhangBean bean : xianbingshiInfos){
			StringBuffer newchunkContent = processingTrainDataSet(bean.getSubxianbingshi());
			String fileName=String.valueOf(bean.getId());
			File f=TxtOperate.newTxt(filePath, fileName);
			try {
				TxtOperate.writeTxtFile(newchunkContent.toString(), f, true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 把生成的结果拷贝到相应的地方，这次生成之后，剩下的实验就不用管了
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年5月2日 上午12:03:14 
	* @parameter 
	* @return
	* @throws
	 */
	public void doCreateENCRFData(String outputPath,File[] crfFiles,List<XianBingShi_new_zhangBean> xianbingshiInfos){
		Map<String, File> fileMap = new HashMap<>();
		for(File file : crfFiles){
			fileMap.put(file.getName(), file);
		}
		StringBuffer fileName = null;
		for(XianBingShi_new_zhangBean bean : xianbingshiInfos){
			String id = bean.getId()+".txt";
			fileName = new StringBuffer(outputPath).append(File.separator).append(id);
			File newFile = new File(fileName.toString());
			File crfFile = fileMap.get(id);
			try {
				FileUtils.copyFile(crfFile, newFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}


















