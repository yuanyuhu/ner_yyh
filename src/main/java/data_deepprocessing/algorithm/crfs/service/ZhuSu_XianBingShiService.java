package data_deepprocessing.algorithm.crfs.service;

import java.util.List;

import data_deepprocessing.algorithm.crfs.CRFsSingleVersion;
import data_deepprocessing.algorithm.crfs.CreateCRFsDataSet;
import data_deepprocessing.prepareData.beans.XianBingShi_New_Bean;
import data_deepprocessing.prepareData.beans.YYH_XianBingShiBean;
import data_deepprocessing.prepareData.beans.YYH_ZhuSuBean;
import data_deepprocessing.prepareData.beans.ZhuSu_New_Bean;
import data_deepprocessing.prepareData.db.InitSeedDB;
import data_deepprocessing.prepareData.db.SeedWordDB;
import data_deepprocessing.prepareData.db.XianBingShi_NewDB;
import data_deepprocessing.prepareData.db.YYH_XianBingShiDB;
import data_deepprocessing.prepareData.db.YYH_ZhuSuDB;
import data_deepprocessing.prepareData.db.ZhuSu_NewDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午8:01:34 
* @version 1.0  
*/

public class ZhuSu_XianBingShiService {
	private InitSeedDB initSeedDB;
	private XianBingShi_NewDB xianBingShi_NewDB;
	private ZhuSu_NewDB zhuSu_NewDB;
	private CreateCRFsDataSet createCRFsDataSet;
	private CRFsSingleVersion crFsSingleVersion;
	
	public List<String> doGetSeedWord(){
		return initSeedDB.selectAllSeedContent();
	}
	
	public List<ZhuSu_New_Bean> doGetZhuSu(){
		return zhuSu_NewDB.selectAllZhuSu();
	}
	
	public List<XianBingShi_New_Bean> doGetXianBingShi(){
		return xianBingShi_NewDB.selectTag1XianBingShi();
	}
	
	static {
		String path = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\主诉做训练集，现病史做测试集\\ExperimentDataSet_new\\";
		trainPath = path + "train";
		testPath = path + "test";
		resultPath = path + "result";
		modelPath = path + "model";
		standardPath=path +"StandardDataSet";
		dicPath = System.getProperty("user.dir") + "\\crf_dic";
	}
	public final static String trainPath; // 训练集位置
	public final static String testPath; // 测试集位置
	public final static String resultPath; // 结果存放位置
	public final static String modelPath; // 生成的模型存放的位置
	public final static String dicPath; // 字典所在的位置
	public final static String standardPath; // 字典所在的位置
	
	
	
	public void generateStandardDataSet(){
		List<String> initSeeds = doGetSeedWord();
		List<XianBingShi_New_Bean> xianbingshiInfos = doGetXianBingShi();
		try {
			createCRFsDataSet.doCreateCrfDataUpGradeThree(standardPath, initSeeds, xianbingshiInfos, "S");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void generateTrainDataSet(){
		List<String> initSeeds = doGetSeedWord();
		List<ZhuSu_New_Bean> zhusuInfos = doGetZhuSu();
		try {
			createCRFsDataSet.doCreateCrfDataUpGradeThree2ZhuSu(trainPath, initSeeds, zhusuInfos, "S");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generateTestDataSet(){
		List<XianBingShi_New_Bean> xianbingshiInfos = doGetXianBingShi();
		try {
			createCRFsDataSet.doCreateCRFTest(testPath, xianbingshiInfos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public void doRunCRFs(){
		try {
			crFsSingleVersion.callDirCRFs(trainPath, testPath, dicPath, modelPath, resultPath, 30);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	public XianBingShi_NewDB getXianBingShi_NewDB() {
		return xianBingShi_NewDB;
	}

	public void setXianBingShi_NewDB(XianBingShi_NewDB xianBingShi_NewDB) {
		this.xianBingShi_NewDB = xianBingShi_NewDB;
	}

	public ZhuSu_NewDB getZhuSu_NewDB() {
		return zhuSu_NewDB;
	}

	public void setZhuSu_NewDB(ZhuSu_NewDB zhuSu_NewDB) {
		this.zhuSu_NewDB = zhuSu_NewDB;
	}

	public CreateCRFsDataSet getCreateCRFsDataSet() {
		return createCRFsDataSet;
	}
	public void setCreateCRFsDataSet(CreateCRFsDataSet createCRFsDataSet) {
		this.createCRFsDataSet = createCRFsDataSet;
	}

	public CRFsSingleVersion getCrFsSingleVersion() {
		return crFsSingleVersion;
	}

	public void setCrFsSingleVersion(CRFsSingleVersion crFsSingleVersion) {
		this.crFsSingleVersion = crFsSingleVersion;
	}

	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}

	
	

}
