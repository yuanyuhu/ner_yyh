package data_deepprocessing.algorithm.crfs.service;

import java.util.List;

import data_deepprocessing.algorithm.crfs.CRFsCrossValid_tool;
import data_deepprocessing.algorithm.crfs.CreateCRFsDataSet;
import data_deepprocessing.prepareData.beans.XianBingShi_New_Bean;
import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.InitSeedDB;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 上午10:19:04 
* @version 1.0  
*/

public class CRF_Templates2CrossValidService {
	
	private CreateCRFsDataSet createCRFsDataSet;
	private CRFsCrossValid_tool crFsCrossValid_tool;
	private InitSeedDB initSeedDB;
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	
	//模板1
//	private static final String dirPath = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\CRFs不同的模板下的表现\\ExperimentDataSet\\YuanYuhuExperimentDataSet_crossvalid_1\\dirpath";
	//模板2
//	private static final String dirPath = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\CRFs不同的模板下的表现\\ExperimentDataSet\\YuanYuhuExperimentDataSet_crossvalid_2\\dirpath";
	//模板3
//	private static final String dirPath = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\CRFs不同的模板下的表现\\ExperimentDataSet\\YuanYuhuExperimentDataSet_crossvalid_3\\dirpath";
	//模板4
//	private static final String dirPath = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\CRFs不同的模板下的表现\\ExperimentDataSet\\YuanYuhuExperimentDataSet_crossvalid_4\\dirpath";
	private static final String dirPath = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\CRFs不同的模板下的表现\\ExperimentDataSet\\YuanYuhuExperimentDataSet_crossvalid_1200\\dirpath";
	
	
	public List<XianBingShi_new_zhangBean> doGetZhangXianBingShiList(){
		return xianBingShi_New_zhangDB.selectALlXianBingShi();
	}
	
	public List<String> doGetSeedWord(){
		return initSeedDB.selectAllSeedContent();
	}
	
	public void generateStandardDataSet(){
		List<String> initSeeds = doGetSeedWord();
		List<XianBingShi_new_zhangBean> xianbingshiInfos = doGetZhangXianBingShiList();
		try {
			createCRFsDataSet.doCreateCrfDataUpGradeThree2CRFNew(dirPath, initSeeds, xianbingshiInfos, "S");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void doRunCrossValid(){
		crFsCrossValid_tool.function();
	}

	public CreateCRFsDataSet getCreateCRFsDataSet() {
		return createCRFsDataSet;
	}

	public void setCreateCRFsDataSet(CreateCRFsDataSet createCRFsDataSet) {
		this.createCRFsDataSet = createCRFsDataSet;
	}

	public CRFsCrossValid_tool getCrFsCrossValid_tool() {
		return crFsCrossValid_tool;
	}

	public void setCrFsCrossValid_tool(CRFsCrossValid_tool crFsCrossValid_tool) {
		this.crFsCrossValid_tool = crFsCrossValid_tool;
	}

	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}
	

}











