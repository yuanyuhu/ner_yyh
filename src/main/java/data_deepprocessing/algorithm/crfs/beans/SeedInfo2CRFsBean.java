package data_deepprocessing.algorithm.crfs.beans;

public class SeedInfo2CRFsBean implements Comparable<SeedInfo2CRFsBean>{
	private String content;
	private Integer startPro;
	private Integer length;
	private Integer sumLength;
	
	public  SeedInfo2CRFsBean() {
		// TODO Auto-generated constructor stub
	}
	
	public  SeedInfo2CRFsBean(String content) {
		// TODO Auto-generated constructor stub
		this.content = content;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((startPro == null) ? 0 : startPro.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeedInfo2CRFsBean other = (SeedInfo2CRFsBean) obj;
		if (startPro == null) {
			if (other.startPro != null)
				return false;
		} else if (!startPro.equals(other.startPro))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "InitSeedInfo [content=" + content + ", startPro=" + startPro + ", length=" + length + ", sumLength="
				+ sumLength + "]";
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getStartPro() {
		return startPro;
	}
	public void setStartPro(Integer startPro) {
		this.startPro = startPro;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getSumLength() {
		return sumLength;
	}
	public void setSumLength(Integer sumLength) {
		this.sumLength = sumLength;
	}

	@Override
	public int compareTo(SeedInfo2CRFsBean o) {
		// TODO Auto-generated method stub
		SeedInfo2CRFsBean beanTwo = (SeedInfo2CRFsBean) o;
		int flag1 = (this.getSumLength() - beanTwo.getSumLength());
		int flag2 = (this.getStartPro() - beanTwo.getStartPro());
		if (flag1 != 0) {
			return flag1;
		} if(flag2 != 0){
			return flag2;
		}else{
			return 0;
		}
	}
	

}
