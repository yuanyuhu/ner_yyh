package data_deepprocessing.algorithm.crfs.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.crfs.service.ZhuSu_XianBingShiService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午7:38:58 
* @version 1.0  
*/

public class Main2_ZhuSu_XianBingShi {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ZhuSu_XianBingShiService service = (ZhuSu_XianBingShiService) context.getBean("zhuSu_XianBingShiService");
		try {
			service.generateStandardDataSet();
			service.generateTrainDataSet();
			service.generateTestDataSet();
			service.doRunCRFs();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
