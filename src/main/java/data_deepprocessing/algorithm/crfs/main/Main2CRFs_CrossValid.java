package data_deepprocessing.algorithm.crfs.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.crfs.service.CRF_Templates2CrossValidService;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月21日 上午8:37:03 
* @version 1.0  
* 
* 
* 
*/

public class Main2CRFs_CrossValid {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		CRF_Templates2CrossValidService service = (CRF_Templates2CrossValidService) context.getBean("cRF_Templates2CrossValidService");
		try {
//			service.generateStandardDataSet();
			service.doRunCrossValid();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
