package data_deepprocessing.algorithm.crfs.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.crfs.service.ShouZhen_FuZhenService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午7:39:22 
* @version 1.0  
*/

public class Main2_ShouZhen_FuZhen {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ShouZhen_FuZhenService service = (ShouZhen_FuZhenService) context.getBean("shouZhen_FuZhenService");
		try {
			service.generateStandardDataSet();
			service.generateTrainDataSet();
			service.generateTestDataSet();
			service.doRunCRFs();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
