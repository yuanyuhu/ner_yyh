package data_deepprocessing.algorithm.crfs;

import java.io.File;
import java.util.List;

import data_deepprocessing.prepareData.db.SeedWordDB;
import data_deepprocessing.prepareData.db.SymptomDB;
import data_deepprocessing.util.FileUtils;
import data_deepprocessing.util.TxtOperate;

/**
 * @author 作者 : YUHU YUAN
 * @date 创建时间：2016年8月11日 上午9:32:20
 * @version 1.0
 * 注意：iterations常设置为30
 */
public class CRFsSingleVersion {


	private SeedWordDB seedWordDB;
	private CrfApp crfApp;

	
	public SeedWordDB getSeedWordDB() {
		return seedWordDB;
	}

	public void setSeedWordDB(SeedWordDB seedWordDB) {
		this.seedWordDB = seedWordDB;
	}

	public CrfApp getCrfApp() {
		return crfApp;
	}

	public void setCrfApp(CrfApp crfApp) {
		this.crfApp = crfApp;
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 下午10:53:19
	 * @parameter	一般取 10
	 * @return
	 * @throws 直接使用路径来调用
	 */
	public void callDirCRFs(String trainPath, String testPath, String dicPath,String modelPath,String resultPath,int iterations) throws Exception {
		regenerateWordFeatureTable(dicPath);
		crfApp.loadData(trainPath, testPath, dicPath, null);
		crfApp.trainModel(iterations, modelPath);
		crfApp.testModel(resultPath, modelPath, false);
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 下午10:53:46
	 * @parameter
	 * @return
	 * @throws 使用文本链表调用程序
	 */
	public void callDefaultParamListCRFs( String dicPath,String modelPath,String resultPath,List<File> trainList, List<File> testList, int iterations) throws Exception {
		regenerateWordFeatureTable(dicPath);
		crfApp.loadData(trainList, testList, dicPath, null);
		crfApp.trainModel(iterations, modelPath);
		crfApp.testModel(resultPath, modelPath, false);

	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 下午10:53:46
	 * @parameter
	 * @return
	 * @throws 使用文本链表调用程序
	 */
	public void callListCRFs(List<File> trainList, List<File> testList, String resultPath, String modelPath,
			String dicPath, int iterations) throws Exception {
		regenerateWordFeatureTable(dicPath);
		crfApp.loadData(trainList, testList, dicPath, null);
		crfApp.trainModel(iterations, modelPath);
		crfApp.testModel(resultPath, modelPath, false);

	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 下午11:03:38
	 * @parameter
	 * @return
	 * @throws 根据字典表生成字或者词相关的特征
	 */
	private void regenerateWordFeatureTable(String dicPath) throws Exception {
		FileUtils fu = new FileUtils();
		List<String> initSeeds =seedWordDB.selectHandle_SeedWord();
		System.out.println(initSeeds.size() + "初始种子词，这里是有问题的，先这样做一下看一下效果吧。");
		fu.delFile(dicPath + "seeddic.txt");
		fu.delFile(dicPath + "B-S.txt");
		fu.delFile(dicPath + "E-S.txt");
		File bsfile = TxtOperate.newTxt(dicPath, "B-S");
		File esfile = TxtOperate.newTxt(dicPath, "E-S");
		File seeddicfile = TxtOperate.newTxt(dicPath, "seeddic");

		for (String trainSeed : initSeeds) {
			String seedLable = "S";
			if (seedLable.equals("S")) {
				String str = trainSeed.substring(0, 1);
				TxtOperate.writeTxtFile(str + "\r\n", bsfile, true);
				TxtOperate.writeTxtFile(str + "\tB-S" + "\r\n", seeddicfile, true);
				for (int j = 1; j <= trainSeed.length() - 1; j++) {
					String str1 = trainSeed.substring(j, j + 1);
					TxtOperate.writeTxtFile(str1 + "\r\n", esfile, true);
					TxtOperate.writeTxtFile(str1 + "\tE-S" + "\r\n", seeddicfile, true);
				}
			}
		}

	}

	/**
	 * 这里可是能完整的把CRFs给跑了
	 */
//	public static void main(String[] args) throws Exception {
//		CRFsSingleVersion crFsSingleVersion = new CRFsSingleVersion();
//		crFsSingleVersion.callDirCRFs(30);
//	}

}
