package data_deepprocessing.algorithm.node2vec.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data_deepprocessing.algorithm.node2vec.bean.WordGraphBean;
import data_deepprocessing.prepareData.beans.Node2vec_WordBookBean;
import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.Node2vec_WordBookDB;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;
import data_deepprocessing.util.FilePathUtil;
import data_deepprocessing.util.FileUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月12日 上午9:50:12 
* @version 1.0  
*/

public class Node2vec_WordGraphService {
	
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	private Node2vec_WordBookDB node2vec_WordBookDB;
	
	private List<XianBingShi_new_zhangBean> doGetAllXianBingShi(){
		return xianBingShi_New_zhangDB.selectALlXianBingShi();
	}
	
	private List<XianBingShi_new_zhangBean> doGetTop20XianBingShi(){
		return xianBingShi_New_zhangDB.selectTop2XianBingShi();
	}
	
	private List<Node2vec_WordBookBean> doGetWordBookList(){
		return node2vec_WordBookDB.selectAllWordBook2Word();
	}
	

	private static List<WordGraphBean> wordGraphBeans = new ArrayList<>();
	private static Map<WordGraphBean, Double> computeWeightMap = new HashMap<>();
	//为了形成字典，因为算法必须要用int来代替
	private static Map<Character, Integer> WordBookMap = new HashMap<>();
	private static Map<String, Integer> Word_NumMap = new HashMap<>();
//	private static Map<Integer, String> Num_WordMap = new HashMap<>();
	
	
	
	private void  getWordPair(XianBingShi_new_zhangBean bean){
		char[] chars = bean.getSubxianbingshi().toCharArray();
		List<Character> noKongGeChars = new ArrayList<>();
		for(char c:chars){
			String cString = String.valueOf(c);
			if(cString==null||cString.equals(" ")|| cString.equals("\t")){
				continue;
			}
			noKongGeChars.add(c);
		}
		for(int i=0; i< noKongGeChars.size()-1; ++i){
			WordBookMap.put(noKongGeChars.get(i), 1);
			WordGraphBean wordGraphBean = new WordGraphBean();
			wordGraphBean.setWordOne(String.valueOf(noKongGeChars.get(i)));
			wordGraphBean.setWordTwo(String.valueOf(noKongGeChars.get(i+1)));
			if(computeWeightMap.containsKey(wordGraphBean)){
				double value = computeWeightMap.get(wordGraphBean).doubleValue();
				++value;
				computeWeightMap.put(wordGraphBean, value);
			}else{
				computeWeightMap.put(wordGraphBean, 1.0);
			}
		}
		WordBookMap.put(chars[chars.length-1], 1);
	}
	
	
	//归一化处理
	private void normalization(){
		double maxValue = 0.0;
		double minValue = 0.0;
		for(WordGraphBean bean : computeWeightMap.keySet()){
			double value = computeWeightMap.get(bean);
			if(value>maxValue){
				maxValue = value;
			}
			if(value<minValue){
				minValue = value;
			}
		}
		double fenmu =  maxValue - minValue;
		for(WordGraphBean bean : computeWeightMap.keySet()){
			double value = computeWeightMap.get(bean);
			double weight = (value - minValue)/fenmu;
			bean.setWeight(weight);
			wordGraphBeans.add(bean);
		}
	}
	
	private void doGenerateWorkBook(){
		int count =1;
		for(Character word : WordBookMap.keySet()){
			Node2vec_WordBookBean bean = new Node2vec_WordBookBean();
			bean.setWord(word.toString());
			bean.setNum(count);
			node2vec_WordBookDB.insert2WordBook2Word(bean);
			count++;
		}
	}
	
	private void doGenerateWordBookMap(){
		List<Node2vec_WordBookBean> list = doGetWordBookList();
		for(Node2vec_WordBookBean bean : list){
			Word_NumMap.put(bean.getWord(), bean.getNum());
		}
	}
	
	
	public void writeResult2File() throws IOException{
		BufferedWriter writer = FileUtil.getWriter(FilePathUtil.node2vec_GephiPath2Word);
		StringBuffer result = new StringBuffer();
		for(WordGraphBean bean : wordGraphBeans){
			int wordOneNum = Word_NumMap.get(bean.getWordOne());
			int wordTwoNum = Word_NumMap.get(bean.getWordTwo());
			result.append(wordOneNum).append(" ")
					.append(wordTwoNum).append(" ")
					.append(bean.getWeight());
			writer.write(result.toString());
			writer.flush();
			writer.newLine();
			result.setLength(0);
		}
		if(writer!=null){
			writer.close();
		}
	}
	
	
	public void function() throws IOException{
		List<XianBingShi_new_zhangBean> xianBingShiList = doGetTop20XianBingShi();
		for(XianBingShi_new_zhangBean bean : xianBingShiList){
			//形成字对Map和字典Map
			getWordPair(bean);
		}
		//对字典对进行归一化处理，得到字典对的权重
		normalization();
		//把字典放到数据库中
		doGenerateWorkBook();
		//查出来字典信息形成
		doGenerateWordBookMap();
		//输出结果，因为必须为int而不是字来代表node,所以才有了以上的操作。
		writeResult2File();
	}
	
	
	
	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}

	public Node2vec_WordBookDB getNode2vec_WordBookDB() {
		return node2vec_WordBookDB;
	}

	public void setNode2vec_WordBookDB(Node2vec_WordBookDB node2vec_WordBookDB) {
		this.node2vec_WordBookDB = node2vec_WordBookDB;
	}
	

}








