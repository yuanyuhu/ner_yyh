package data_deepprocessing.algorithm.node2vec.bean;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月12日 上午9:53:41 
* @version 1.0  
*/

public class WordGraphBean  implements Comparable<WordGraphBean>{
	
	private String WordOne;
	private String WordTwo;
	private double weight;
	
	public WordGraphBean(){}
	
	public WordGraphBean(String wordOne, String wordTwo, double weight) {
		super();
		WordOne = wordOne;
		WordTwo = wordTwo;
		this.weight = weight;
	}
	
	public String getWordOne() {
		return WordOne;
	}
	public void setWordOne(String wordOne) {
		WordOne = wordOne;
	}
	public String getWordTwo() {
		return WordTwo;
	}
	public void setWordTwo(String wordTwo) {
		WordTwo = wordTwo;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((WordOne == null) ? 0 : WordOne.hashCode());
		result = prime * result + ((WordTwo == null) ? 0 : WordTwo.hashCode());
		return result;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordGraphBean other = (WordGraphBean) obj;
		if (WordOne == null) {
			if (other.WordOne != null)
				return false;
		} else if (!WordOne.equals(other.WordOne))
			return false;
		if (WordTwo == null) {
			if (other.WordTwo != null)
				return false;
		} else if (!WordTwo.equals(other.WordTwo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "WordGraphBean [WordOne=" + WordOne + ", WordTwo=" + WordTwo + ", weight=" + weight + "]";
	}
	@Override
	public int compareTo(WordGraphBean o) {
		// TODO Auto-generated method stub
		double value = this.getWeight()- o.getWeight();
		if(value>=0){
			return 1;
		}else{
			return -1;
		}
	}
	

}

































