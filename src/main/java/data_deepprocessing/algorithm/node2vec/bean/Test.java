package data_deepprocessing.algorithm.node2vec.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月12日 上午10:18:39 
* @version 1.0  
*/

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WordGraphBean bean1 = new WordGraphBean();
		WordGraphBean bean2 = new WordGraphBean();
		bean1.setWordOne("袁玉虎");
		bean1.setWordTwo("张振");
		bean1.setWeight(0.45);
		bean2.setWordOne("袁玉虎");
		bean2.setWordTwo("张振");
		bean2.setWeight(0.23);
		
		if(bean1.equals(bean2)){
			System.out.println("yes");
		}
		
		Set<WordGraphBean> set = new HashSet<>();
		set.add(bean1);
		set.add(bean2);
		System.out.println(set.size());
		
		List<WordGraphBean> list = new ArrayList<>();
		list.add(bean1);
		list.add(bean2);
		
		for(WordGraphBean bean : list){
			System.out.println(bean);
		}
		
		Collections.sort(list);
		
		for(WordGraphBean bean : list){
			System.out.println(bean);
		}
		

	}

}















