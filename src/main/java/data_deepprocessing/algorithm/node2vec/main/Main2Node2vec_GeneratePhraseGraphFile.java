package data_deepprocessing.algorithm.node2vec.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.node2vec.service.Node2vec_PhraseGraphService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月12日 上午11:42:15 
* @version 1.0  
*/

public class Main2Node2vec_GeneratePhraseGraphFile {
	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		Node2vec_PhraseGraphService service = (Node2vec_PhraseGraphService) context.getBean("node2vec_PhraseGraphService");
		
		service.function();
		

	}

}
