package data_deepprocessing.algorithm.ssvm.ssvm_util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deeplearning4j.models.word2vec.Word2Vec;
import data_deepprocessing.algorithm.word_embedding.service.YYH_Word2VectorService;
import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.InitSeedDB;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;
import data_deepprocessing.util.FileUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月10日 下午8:06:30 
* @version 1.0  
*/

public class CRFplusCrossValidService {
	
	private InitSeedDB initSeedDB;
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	
	private List<XianBingShi_new_zhangBean> doGetSegmentedXianBingShi(){
		return xianBingShi_New_zhangDB.selectALlXianBingShi();
	}
	
	private Word2Vec doGetWord2Vec(){
		
		try {
			return YYH_Word2VectorService.getWord2Vec();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private List<String> doGetSymptom_Dictionary(){
		return initSeedDB.selectAllSeedContent();
	}
	
	
	private void divideDataset(){
		List<XianBingShi_new_zhangBean> beanList = doGetSegmentedXianBingShi();
		while (!beanList.isEmpty()) {
			int size = beanList.size();
			int index = (int) (Math.random() * size);
			XianBingShi_new_zhangBean tmp = beanList.get(index);
			divided_dataset.get(size % 10).add(tmp);
			beanList.remove(index);
		}
		
	}
	
	
	private Map<Integer, List<XianBingShi_new_zhangBean>> divided_dataset = initMap();
	
	private Map<Integer, List<XianBingShi_new_zhangBean>> initMap(){
		Map<Integer, List<XianBingShi_new_zhangBean>> divided_dataset = new HashMap<>();
		for (int i = 0; i < 10; i++) {
			List<XianBingShi_new_zhangBean> fold = new ArrayList<XianBingShi_new_zhangBean>();
			divided_dataset.put(i, fold);
		}
		return divided_dataset;
	}
	
	
	
	private void doGenerateCRFDataSet(String path,List<XianBingShi_new_zhangBean> xianbingshiBeanList,Word2Vec word2Vec) throws IOException{
		BufferedWriter writer = FileUtil.getWriter(path);
		List<String> symptom_dictionary = doGetSymptom_Dictionary(); //这里换成map会不会查找的更快
		StringBuffer constructTerm = new StringBuffer();
		for(XianBingShi_new_zhangBean bean : xianbingshiBeanList){
			String segmentedXBS = bean.getContent_segmented();
			if(segmentedXBS.length()<20){
				continue;
			}
			String[] wordOrPhrases = segmentedXBS.trim().split("[\\s]+");
			for(String word:wordOrPhrases){
				if(word==null && word.equals("")&&word.trim().equals("")){
					continue;
				}
				double[] voctor = word2Vec.getWordVector(word);
				String tag = (symptom_dictionary.contains(word)?1:2)+"";
				constructTerm.append(tag).append("\t");
				if(voctor!=null ){
					for(int i=0; i<voctor.length; ++i){
						constructTerm.append((i+1)+":"+voctor[i]).append("\t");
					}
				}else{
					for(int i=0; i<200; ++i){
						constructTerm.append((i+1)+":"+1).append("\t");
					}
				}
				writer.write(constructTerm.toString());
				writer.flush();
				writer.newLine();
				constructTerm.setLength(0);
			}
			writer.newLine();
		}
		if(writer!=null){
			writer.close();
		}
		
	}
	
	
	private static String path = "D:\\yyh_yuanyuhu_graduation_experimental\\word_embedding_CRF++\\dataset\\";
	
	
	public void doGenerateCRFCrossValidDataSet(){
		divideDataset();
		Word2Vec word2Vec = doGetWord2Vec();
		
		for(int i=0 ; i<divided_dataset.size(); ++i){
			StringBuffer testPath = new StringBuffer(path).append("data")
					.append(i).append(File.separator).append("test")
					.append(File.separator).append("test.crfsuite.txt");
			List<XianBingShi_new_zhangBean> testDataSet = divided_dataset.get(i);
			try {
				doGenerateCRFDataSet(testPath.toString(),testDataSet, word2Vec);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringBuffer trainPath = new StringBuffer(path).append("data")
					.append(i).append(File.separator).append("train")
					.append(File.separator).append("train.crfsuite.dat");
			List<XianBingShi_new_zhangBean> trainDataSet = new ArrayList<>();
			for(int j=0; j<divided_dataset.size(); ++j){
				if(j!=i){
					trainDataSet.addAll(divided_dataset.get(j));
				}
			}
			try {
				doGenerateCRFDataSet(trainPath.toString(),trainDataSet, word2Vec);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			testPath.setLength(0);
			trainPath.setLength(0);
		}
		
	}
	
	
	
	
	
	
	
	
	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}

}








