package data_deepprocessing.algorithm.ssvm.ssvm_util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data_deepprocessing.algorithm.crfs.CreateCRFsDataSet;
import data_deepprocessing.algorithm.ssvm.bean.WordMarkBean;
import data_deepprocessing.prepareData.beans.Node2vec_WordBookBean;
import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.InitSeedDB;
import data_deepprocessing.prepareData.db.Node2vec_WordBookDB;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;
import data_deepprocessing.util.FilePathUtil;
import data_deepprocessing.util.FileUtil;

/**
 * @author 作者 : YUHU YUAN
 * @date 创建时间：2017年4月10日 下午8:06:30
 * @version 1.0 他跟word2vec的区别就是多一个读vector文件的过程
 */

public class CRFplusCrossValidService2Node2vec2Word {

	private InitSeedDB initSeedDB;
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	private Node2vec_WordBookDB node2vec_WordBookDB;
	private CreateCRFsDataSet createCRFsDataSet;

	/**
	 * 下面是得到 word & vector模块 以下是添加的步骤
	 */
	private List<Node2vec_WordBookBean> doGetWordBookBeans() {
		return node2vec_WordBookDB.selectAllWordBook2Word();
	}

	private static Map<Integer, String> Num_WordMap = new HashMap<>();

	private static Map<String, String> Word_VectorMap = new HashMap<>();

	private void doBuildWord_VectorMap() throws IOException {
		List<Node2vec_WordBookBean> node2vec_WordBookBeans = doGetWordBookBeans();
		for (Node2vec_WordBookBean bean : node2vec_WordBookBeans) {
			Num_WordMap.put(bean.getNum(), bean.getWord());
		}

		BufferedReader reader = FileUtil.getReader(FilePathUtil.node2vec_vectorPath2Word);
		String line = "";
		line = reader.readLine();// 第一行不是向量数据不要
		while ((line = reader.readLine()) != null) {
			String[] tempStrings = line.split(" ", 2);
			Word_VectorMap.put(Num_WordMap.get(Integer.valueOf(tempStrings[0])), tempStrings[1]);
		}
	}

	/**
	 * 
	 * 以上为添加的步骤
	 */

	private List<XianBingShi_new_zhangBean> doGetSegmentedXianBingShi() {
		return xianBingShi_New_zhangDB.selectALlXianBingShi();
	}

	private List<String> doGetSymptom_Dictionary() {
		return initSeedDB.selectAllSeedContent();
	}

	private void divideDataset() {
		List<XianBingShi_new_zhangBean> beanList = doGetSegmentedXianBingShi();
		while (!beanList.isEmpty()) {
			int size = beanList.size();
			int index = (int) (Math.random() * size);
			XianBingShi_new_zhangBean tmp = beanList.get(index);
			divided_dataset.get(size % 10).add(tmp);
			beanList.remove(index);
		}

	}

	private Map<Integer, List<XianBingShi_new_zhangBean>> divided_dataset = initMap();

	private Map<Integer, List<XianBingShi_new_zhangBean>> initMap() {
		Map<Integer, List<XianBingShi_new_zhangBean>> divided_dataset = new HashMap<>();
		for (int i = 0; i < 10; i++) {
			List<XianBingShi_new_zhangBean> fold = new ArrayList<XianBingShi_new_zhangBean>();
			divided_dataset.put(i, fold);
		}
		return divided_dataset;
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年4月13日 下午3:48:31
	 * @parameter
	 * @return
	 * @throws 现在已字为模型就不能这么做了，它需要查两次字典：
	 *             1：word voctor 2: word B-S或者E-S 这里开头的数字只是类别，跟
	 */
	private void doGenerateCRFDataSet(String outputPath, String inputPath) throws IOException {
		File[] crfFiles = new File(inputPath).listFiles();
		BufferedWriter writer = FileUtil.getWriter(outputPath);
		List<WordMarkBean> wordMarkBeans = null;
		BufferedReader reader = null;
		StringBuffer constructTerm = new StringBuffer();
		for (File file : crfFiles) {
			reader = FileUtil.getReader(file);
			wordMarkBeans = new ArrayList<>();
			String line = "";
			while ((line = reader.readLine()) != null) {
				String[] temp = line.split("\t");
				WordMarkBean bean = new WordMarkBean(temp[0], temp[1]);
				wordMarkBeans.add(bean);
			}
			for (WordMarkBean bean : wordMarkBeans) {
				String[] voctor = Word_VectorMap.get(bean.getWord()).split(" ");
				String mark = bean.getMark();
				String tag = "";
				if (mark.equals("O")) {
					tag = "1";
				} else if (mark.equals("B-S")) {
					tag = "2";
				} else if (mark.equals("E-S")) {
					tag = "3";
				}
				constructTerm.append(tag).append("\t");
				if (voctor != null) {
					for (int i = 0; i < voctor.length; ++i) {
						constructTerm.append((i + 1) + ":" + voctor[i]).append("\t");
					}
				} else {
					for (int i = 0; i < 200; ++i) {
						constructTerm.append((i + 1) + ":" + 1).append("\t");
					}
				}
				writer.write(constructTerm.toString());
				writer.flush();
				writer.newLine();
				constructTerm.setLength(0);
			}
			writer.newLine();

		}
		if(reader!=null){
			reader.close();
		}
		
		if (writer != null) {
			writer.close();
		}

	}

	private static String path = "D:\\yyh_yuanyuhu_graduation_experimental\\node2vec_CRF++\\dataset2word\\";

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年4月13日 下午3:45:11
	 * @parameter
	 * @return
	 * @throws 它目前的功能是生成原始的CRF模型的训练集和测试集，当然这里的测试集跟训练集一样是标注过的词语
	 */
	private void doGenerateCRFCrossValidDataSet() {
		divideDataset();
		List<String> initSeeds = doGetSymptom_Dictionary();
		for (int i = 0; i < divided_dataset.size(); ++i) {
			StringBuffer testPath = new StringBuffer(path).append("dataCRF").append(i).append(File.separator).append("test");

			List<XianBingShi_new_zhangBean> testDataSet = divided_dataset.get(i);
			try {
				createCRFsDataSet.doCreateCrfDataUpGradeThree2CRFNew(testPath.toString(), initSeeds, testDataSet, "S");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			StringBuffer trainPath = new StringBuffer(path).append("dataCRF").append(i).append(File.separator).append("train");
			List<XianBingShi_new_zhangBean> trainDataSet = new ArrayList<>();
			for (int j = 0; j < divided_dataset.size(); ++j) {
				if (j != i) {
					trainDataSet.addAll(divided_dataset.get(j));
				}
			}
			try {
				createCRFsDataSet.doCreateCrfDataUpGradeThree2CRFNew(trainPath.toString(), initSeeds, trainDataSet,"S");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			testPath.setLength(0);
			trainPath.setLength(0);
		}

	}

	private void doGenerateNode2vec2CRFCrossValidDataSet() {
		try {
			doBuildWord_VectorMap();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int i = 0; i < divided_dataset.size(); ++i) {
			StringBuffer node2vecTestPath = new StringBuffer(path).append("data").append(i).append(File.separator)
					.append("test").append(File.separator).append("test.crfsuite.txt");
			StringBuffer crfTestPath = new StringBuffer(path).append("dataCRF").append(i).append(File.separator)
					.append("test");
			
			try {
				doGenerateCRFDataSet(node2vecTestPath.toString(), crfTestPath.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringBuffer node2vecTrainPath = new StringBuffer(path).append("data").append(i).append(File.separator)
					.append("train").append(File.separator).append("train.crfsuite.dat");
			StringBuffer crfTrainPath = new StringBuffer(path).append("dataCRF").append(i).append(File.separator)
					.append("train");
			List<XianBingShi_new_zhangBean> trainDataSet = new ArrayList<>();
			for (int j = 0; j < divided_dataset.size(); ++j) {
				if (j != i) {
					trainDataSet.addAll(divided_dataset.get(j));
				}
			}
			try {
				doGenerateCRFDataSet(node2vecTrainPath.toString(), crfTrainPath.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			node2vecTestPath.setLength(0);
			crfTestPath.setLength(0);
			node2vecTrainPath.setLength(0);
			crfTrainPath.setLength(0);
		}

	}
	
	public void function(){
		doGenerateCRFCrossValidDataSet();
		doGenerateNode2vec2CRFCrossValidDataSet();
	}
	

	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}

	public Node2vec_WordBookDB getNode2vec_WordBookDB() {
		return node2vec_WordBookDB;
	}

	public void setNode2vec_WordBookDB(Node2vec_WordBookDB node2vec_WordBookDB) {
		this.node2vec_WordBookDB = node2vec_WordBookDB;
	}

	public CreateCRFsDataSet getCreateCRFsDataSet() {
		return createCRFsDataSet;
	}

	public void setCreateCRFsDataSet(CreateCRFsDataSet createCRFsDataSet) {
		this.createCRFsDataSet = createCRFsDataSet;
	}

}
