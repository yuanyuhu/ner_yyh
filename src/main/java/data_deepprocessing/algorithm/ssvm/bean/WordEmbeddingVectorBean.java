package data_deepprocessing.algorithm.ssvm.bean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午7:45:22 
* @version 1.0  
*/

public class  WordEmbeddingVectorBean{
	
	private String word;
	private String Vector;
	
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getVector() {
		return Vector;
	}
	public void setVector(String vector) {
		Vector = vector;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordEmbeddingVectorBean other = (WordEmbeddingVectorBean) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "WordEmbeddingVectorBean [word=" + word + ", Vector=" + Vector + "]";
	}

}













