package data_deepprocessing.algorithm.ssvm.bean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月13日 下午4:06:10 
* @version 1.0  
*/

public class WordMarkBean {
	private String word;
	private String mark;
	
	
	public WordMarkBean(String word, String mark) {
		super();
		this.word = word;
		this.mark = mark;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mark == null) ? 0 : mark.hashCode());
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordMarkBean other = (WordMarkBean) obj;
		if (mark == null) {
			if (other.mark != null)
				return false;
		} else if (!mark.equals(other.mark))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "WordMarkBean [word=" + word + ", mark=" + mark + "]";
	}
	
	

}
