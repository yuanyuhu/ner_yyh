package data_deepprocessing.algorithm.ssvm.evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import data_deepprocessing.util.FileUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月10日 上午8:54:09 
* @version 1.0  
*/

public class ResultEvaluateMain {
	
	public static String path = "D:\\yyh_yuanyuhu_graduation_experimental\\node2vec_SSVM\\dataset2word\\";
	
	public void evaluateFunction() throws IOException{
		for(int i=0; i<10; ++i){
			double correct = 0;
			// 抽取的实体总数
			double extract = 0;
			// 标准病历中实体的个数
			double total = 0;
			
			
			StringBuffer standardFilePath = new StringBuffer(path).append("data")
					.append(i).append(File.separator).append("test")
					.append(File.separator).append("test2Judge.txt");
			StringBuffer resultFilePath = new StringBuffer(path).append("data")
					.append(i).append(File.separator).append("result.dat");
			StringBuffer evaluateFilePath = new StringBuffer(path).append("data")
					.append(i).append(File.separator).append("evaluate.txt");
			
			BufferedReader standardFileReader = FileUtil.getReader(standardFilePath.toString());
			
			BufferedReader resultFileReader = FileUtil.getReader(resultFilePath.toString());
			
			BufferedWriter evaluateFileWriter = FileUtil.getWriter(evaluateFilePath.toString());
		
			
			List<String> standardList = new ArrayList<>();
			List<String> resultList = new ArrayList<>();
			String line = null;
			while((line = standardFileReader.readLine())!=null){
				standardList.add(line.substring(0, line.indexOf(" ")).trim());
			}
			
			while((line = resultFileReader.readLine())!=null){
				resultList.add(line.trim());
			}
			
			for(int j=0; j<standardList.size(); ++j){
				int standard = Integer.valueOf(standardList.get(j));
				int result = Integer.valueOf(resultList.get(j));
				if(standard == 1){
					++total;
				}
				if(standard==1 && result ==1){
					++correct;
				}
				if(result == 1){
					++extract;
				}
			}
			
			double accuracy = 0.0;
			double recall = 0.0;
			double f1 = 0.0;
			
			if(extract==0.0){
				accuracy=0.0;
			}else{
				accuracy = correct/extract;
			}

			if(total==0.0){
				recall=0.0;
			}else{
				recall = correct/total;
			}

			if(accuracy+recall==0.0){
				f1=0.0;
			}else{
				f1 = (2*accuracy*recall)/(accuracy+recall);
			} 
			
			
			StringBuffer writeResult = new StringBuffer();
			writeResult.append("result：").append(" 准确率： ").append(accuracy).append(" ")
										.append("召回率：").append(recall).append(" ")
										.append("F1：").append(f1).append(" ");
			
			evaluateFileWriter.write(writeResult.toString());
			evaluateFileWriter.flush();
			
			if(evaluateFileWriter!=null){
				evaluateFileWriter.close();
			}
			if(standardFileReader!=null){
				standardFileReader.close();
			}
			if(resultFileReader!=null){
				resultFileReader.close();
			}
		
		}
		
	}
	
	
	public static void main(String[] args){
		ResultEvaluateMain resultEvaluate = new ResultEvaluateMain();
		try {
			resultEvaluate.evaluateFunction();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}





















