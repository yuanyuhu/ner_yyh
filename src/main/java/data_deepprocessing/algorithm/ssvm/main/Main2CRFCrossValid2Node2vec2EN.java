package data_deepprocessing.algorithm.ssvm.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.ssvm.ssvm_util.CRFplusCrossValidService2Node2vec2EN;
import data_deepprocessing.algorithm.ssvm.ssvm_util.CRFplusCrossValidService2Node2vec2Word;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 下午11:08:12 
* @version 1.0  
*/

public class Main2CRFCrossValid2Node2vec2EN {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		CRFplusCrossValidService2Node2vec2EN service = (CRFplusCrossValidService2Node2vec2EN) context.getBean("cRFplusCrossValidService2Node2vec2EN");
		
		service.doGenerateNode2vec2CRFCrossValidDataSet();
		
	}

}
