package data_deepprocessing.algorithm.ssvm.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.ssvm.ssvm_util.SsvmService2CrossValidService2Node2vec2EN;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 下午11:08:12 
* @version 1.0  
*/

public class Main2SSVMCrossValid2Node2vec2EN {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		SsvmService2CrossValidService2Node2vec2EN service = (SsvmService2CrossValidService2Node2vec2EN) context.getBean("ssvmService2CrossValidService2Node2vec2EN");
		
		service.doGenerateSsvmCrossValidDataSet();
		
	}

}
