package data_deepprocessing.algorithm.ssvm.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.ssvm.ssvm_util.CRFplusCrossValidService;
import data_deepprocessing.algorithm.ssvm.ssvm_util.SsvmService2CrossValidService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 下午11:08:12 
* @version 1.0  
*/

public class Main2CRFCrossValid {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		CRFplusCrossValidService service = (CRFplusCrossValidService) context.getBean("cRFplusCrossValidService");
		
		service.doGenerateCRFCrossValidDataSet();
		
	}

}
