package data_deepprocessing.algorithm.ssvm.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import data_deepprocessing.util.FilePathUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2016年12月23日 上午11:35:42 
* @version 1.0  
*/

public class Main2SSVM {
	
	/**
	 *  svm_hmm_learn -c 5 -e 0.5 example7/declaration_of_independence.dat declaration.model 
     *	svm_hmm_classify example7/gettysburg_address.dat declaration.model test.outtags 
	 */
	
	public static void generateModel(){
		try {
			System.out.println(" before generateModel()");
			StringBuffer svm_hmm_learnBuffer = new StringBuffer();
			svm_hmm_learnBuffer.append(FilePathUtil.svm_hmm_learn).append(" ");
			svm_hmm_learnBuffer.append("-c").append(" ");
			svm_hmm_learnBuffer.append("5").append(" ");
			svm_hmm_learnBuffer.append("-e").append(" ");
			svm_hmm_learnBuffer.append("0.5").append(" ");
			svm_hmm_learnBuffer.append(FilePathUtil.SSVM_INPUT).append(" ");
			svm_hmm_learnBuffer.append(FilePathUtil.SSVM_MODEL).append(" ");
			System.out.println(svm_hmm_learnBuffer);
			Runtime.getRuntime().exec(svm_hmm_learnBuffer.toString());
//			Runtime.getRuntime().exec(new String(FilePathUtil.svm_hmm_learn+ " -c 5 -e 0.5 "
//					+ "D:\\ssvm_test/example7/declaration_of_independence.dat D:\\ssvm_test\\declaration.model"));
			System.out.println(" after generateModel()");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void generateResult(){
		try {
			System.out.println(" before generateResult()");
			Runtime.getRuntime().exec(new String("D:\\ssvm_test\\svm_hmm_classify D:\\ssvm_test\\example7/gettysburg_address.dat D:\\ssvm_test\\declaration.model D:\\ssvm_test\\test.outtags "));
			System.out.println(" after generateResult()");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void generateTest(){
		try {
			System.out.println(" before generateTest()");
			List<String> commands1 = new ArrayList<>();
			commands1.add("D:\\ssvm_test\\svm_hmm_learn -c 5 -e 0.5 D:\\ssvm_test/example7/declaration_of_independence.dat D:\\ssvm_test\\declaration.model");
			ProcessBuilder pb1 = new ProcessBuilder();
			pb1.command(commands1);
			Process pr1 = pb1.start();
			
			
			List<String> commands2 = new ArrayList<>();
			commands2.add("D:\\ssvm_test\\svm_hmm_classify D:\\ssvm_test\\example7/gettysburg_address.dat D:\\ssvm_test\\declaration.model D:\\ssvm_test\\test.outtags ");
			ProcessBuilder pb2 = new ProcessBuilder();
			pb2.command(commands2);
			Process pr2 = pb2.start();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		generateModel();
	}

}













