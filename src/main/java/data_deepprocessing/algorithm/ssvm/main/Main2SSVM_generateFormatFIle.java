package data_deepprocessing.algorithm.ssvm.main;


import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.ssvm.ssvm_util.SsvmService;

/**
 * @author 作者 : YUHU YUAN
 * @date 创建时间：2017年4月5日 下午8:15:46
 * @version 1.0
 */

public class Main2SSVM_generateFormatFIle {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		SsvmService service = (SsvmService) context.getBean("ssvmService");
		
		service.doGenerateSsvmDataSet();
		

	}

}
