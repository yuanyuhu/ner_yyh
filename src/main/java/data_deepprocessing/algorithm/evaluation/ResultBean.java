package data_deepprocessing.algorithm.evaluation;

public class ResultBean {
	private double accuracy;
	private double recall;
	private double f1;
	private double accuracyB;//B标注正确的准确度
	private double extract;
	private double correct;
	private double total;
	private double correctB;

	public double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	public double getRecall() {
		return recall;
	}
	public void setRecall(double recall) {
		this.recall = recall;
	}
	public double getF1() {
		return f1;
	}
	public void setF1(double f1) {
		this.f1 = f1;
	}
	public double getExtract() {
		return extract;
	}
	public void setExtract(double extract) {
		this.extract = extract;
	}
	public double getCorrect() {
		return correct;
	}
	public void setCorrect(double correct) {
		this.correct = correct;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getCorrectB() {
		return correctB;
	}
	public void setCorrectB(double correctB) {
		this.correctB = correctB;
	}
	public double getAccuracyB() {
		return accuracyB;
	}
	public void setAccuracyB(double accuracyB) {
		this.accuracyB = accuracyB;
	}

}

