package data_deepprocessing.algorithm.evaluation;

import java.io.File;
import java.util.Map;

import data_deepprocessing.util.FileUtils;
import data_deepprocessing.util.TxtOperate;




/**
 * 这里只是给出了计算的结果，目前来看这个类是不会出错的
 */
public class ResultEvaluateMain {
	public static void doEvaluateBootstrapping(String testfilePath,String standardFilePath,String evaluateFilePath){
		try {
			FileUtils fu=new FileUtils();
			File evaluateFile=TxtOperate.newTxt(evaluateFilePath, "evaluate");
			Map<Integer,File> testFile=fu.getFile(testfilePath);
			double correctS=0;
			double correctD=0;
			double correctI=0;
			double extractS=0;
			double extractD=0;
			double extractI=0;
			double totalS=0;
			double totalD=0;
			double totalI=0;
			int totalSFile=testFile.size();
			int totalDFile=testFile.size();
			int totalIFile=testFile.size();
			double totalSpre=0.0;
			double totalSrecall=0.0;
			double totalSf1=0.0;
			double totalDpre=0.0;
			double totalDrecall=0.0;
			double totalDf1=0.0;
			double totalIpre=0.0;
			double totalIrecall=0.0;
			double totalIf1=0.0;
			
			
			for (File file  : testFile.values()) {
				String v=file.getName();
				EvaluateResult er = new EvaluateResult(testfilePath+"/"+v, standardFilePath+"/"+v,evaluateFile);
				TxtOperate.writeTxtFile("文件编号:"+v+"\r\n", evaluateFile, true);
				String[] tagList = {"S","D","I"};
				for(String tag: tagList) {
					ResultBean result = er.evaluateResult(tag);
					double precesion=result.getAccuracy();			
					double recall=result.getRecall();
					double f1=result.getF1();
					if(tag.equals("S")){	
						correctS=correctS+result.getCorrect();
						extractS=extractS+result.getExtract();
						totalS=totalS+result.getTotal();
						totalSpre=totalSpre+precesion;
						totalSrecall=totalSrecall+recall;
						totalSf1=totalSf1+f1;
					}if(tag.equals("D")){
						if((precesion==0.0)&&(recall==0.0)&&(f1==0.0)){
							totalDFile--;
							continue;
						}else{	
							correctD=correctD+result.getCorrect();
							extractD=extractD+result.getExtract();
							totalD=totalD+result.getTotal();
							totalDpre=totalDpre+precesion;
							totalDrecall=totalDrecall+recall;
							totalDf1=totalDf1+f1;
						}
					}if(tag.equals("I")){
						if((precesion==0.0)&&(recall==0.0)&&(f1==0.0)){
							totalIFile--;
							continue;
						}else{
							correctI=correctI+result.getCorrect();
							extractI=extractI+result.getExtract();
							totalI=totalI+result.getTotal();
							totalIpre=totalIpre+precesion;
							totalIrecall=totalIrecall+recall;
							totalIf1=totalIf1+f1;
						}
					}								
				}
			}
			TxtOperate.writeTxtFile("============================================================================================="+"\r\n", evaluateFile, true);
			TxtOperate.writeTxtFile("以下为计算每个病例块的精度，Recall和F1值，然后求得平均值："+"\r\n", evaluateFile, true);	
			TxtOperate.writeTxtFile("症状的Precious= "+totalSpre/totalSFile+"-----症状的Recall="+totalSrecall/totalSFile+"-----症状的F1="+totalSf1/totalSFile+"\r\n", evaluateFile, true);
			TxtOperate.writeTxtFile("疾病的Precious= "+totalDpre/totalDFile+"-----症状的Recall="+totalDrecall/totalDFile+"-----症状的F1="+totalDf1/totalDFile+"\r\n", evaluateFile, true);
			TxtOperate.writeTxtFile("诱因的Precious= "+totalIpre/totalIFile+"-----诱因的Recall="+totalIrecall/totalIFile+"-----症状的F1="+totalIf1/totalIFile+"\r\n", evaluateFile, true);							
			TxtOperate.writeTxtFile("=============================================================================================="+"\r\n", evaluateFile, true);			
			TxtOperate.writeTxtFile("抽取出的症状的实体总和="+extractS+"-----正确抽取出的症状的实体总和="+correctS+"-----标准病历中症状的实体总和="+totalS+"\r\n", evaluateFile, true);	
			TxtOperate.writeTxtFile("抽取出的疾病的实体总和="+extractD+"-----正确抽取出的疾病的实体总和="+correctD+"-----标准病历中疾病的实体总和="+totalD+"\r\n", evaluateFile, true);	
			TxtOperate.writeTxtFile("抽取出的诱因的实体总和="+extractI+"-----正确抽取出的诱因的实体总和="+correctI+"-----标准病历中诱因的实体总和="+totalI+"\r\n", evaluateFile, true);	
			TxtOperate.writeTxtFile("=============================================================================================="+"\r\n", evaluateFile, true);			
			double precisionAllS=correctS/extractS;
			double precisionAllD=correctD/extractD;
			double precisionAllI=correctI/extractI;
			double recallAllS=correctS/totalS;
			double recallAllD=correctD/totalD;
			double recallAllI=correctI/totalI;
			double f1AllS=(2*precisionAllS*recallAllS)/(precisionAllS+recallAllS);
			double f1AllD=(2*precisionAllD*recallAllD)/(precisionAllD+recallAllD);
			double f1AllI=(2*precisionAllI*recallAllI)/(precisionAllI+recallAllI);
			TxtOperate.writeTxtFile("症状的精度="+precisionAllS+"-----症状的recall="+recallAllS+"-----症状的F1值="+f1AllS+"\r\n", evaluateFile, true);
			TxtOperate.writeTxtFile("疾病的精度="+precisionAllD+"-----疾病的recall="+recallAllD+"-----疾病的F1值="+f1AllD+"\r\n", evaluateFile, true);	
			TxtOperate.writeTxtFile("诱因的精度="+precisionAllI+"-----诱因的recall="+recallAllI+"-----诱因的F1值="+f1AllI+"\r\n", evaluateFile, true);	
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		//这个是主诉为训练集，现病史为测试集
//		String path = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\主诉做训练集，现病史做测试集\\ExperimentDataSet_new\\";
//		String path = "D:\\yyh_yuanyuhu_graduation_experimental\\CRFs\\首诊做训练集，其他做测试集\\ExperimentDataSet_Tag1_train";
		String path = "D:\\yyh_yuanyuhu_graduation_experimental\\Bootstrapping\\dataset1";
		String standardFilePath = path + "/StandardDataSet";
		String testFilePath = path + "/result";
		String evaluateFilePath = path + "/evaluate";
		doEvaluateBootstrapping(testFilePath, standardFilePath, evaluateFilePath);
	}
}





