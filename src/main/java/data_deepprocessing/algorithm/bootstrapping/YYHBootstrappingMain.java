package data_deepprocessing.algorithm.bootstrapping;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.bootstrapping.BootstrappingService;
import data_deepprocessing.algorithm.bootstrapping.beans.SentenceBean;




/**
 * @author YUANYUHU
 */
public class YYHBootstrappingMain {
	public static Logger logger = Logger.getLogger(YYHBootstrappingMain.class.getName());
	private BootstrappingService bootstrappingService;
	private int loopTimes;               //迭代次数
	private int frequencyLimit;          //筛选的阈值 
	private int prefixWindow;           //前几个字符
	private int suffixWindow;           //后几个字符
	
	
	public void callBootstrapping(){
		bootstrappingService.generateInitSeed(frequencyLimit);
		List<SentenceBean> sentenceInfos = bootstrappingService.getUsefulSentence();
		for(SentenceBean bean : sentenceInfos){
			System.out.println(bean);
		}
		for(int t=1;t<=loopTimes;++t){
			logger.info("这是第 "+t+ "次迭代。");
			bootstrappingService.bootstrappingMain(sentenceInfos,prefixWindow, suffixWindow, frequencyLimit);
		}
	}
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		YYHBootstrappingMain service = (YYHBootstrappingMain) context.getBean("yyhBootstrappingMain");
		
		service.callBootstrapping();
		 
	}
	
	public BootstrappingService getBootstrappingService() {
		return bootstrappingService;
	}

	public void setBootstrappingService(BootstrappingService bootstrappingService) {
		this.bootstrappingService = bootstrappingService;
	}

	public int getLoopTimes() {
		return loopTimes;
	}

	public void setLoopTimes(int loopTimes) {
		this.loopTimes = loopTimes;
	}

	public int getFrequencyLimit() {
		return frequencyLimit;
	}

	public void setFrequencyLimit(int frequencyLimit) {
		this.frequencyLimit = frequencyLimit;
	}

	public int getPrefixWindow() {
		return prefixWindow;
	}

	public void setPrefixWindow(int prefixWindow) {
		this.prefixWindow = prefixWindow;
	}

	public int getSuffixWindow() {
		return suffixWindow;
	}

	public void setSuffixWindow(int suffixWindow) {
		this.suffixWindow = suffixWindow;
	}

	

}







