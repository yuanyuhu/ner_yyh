package data_deepprocessing.algorithm.bootstrapping.service;

import java.util.List;

import data_deepprocessing.algorithm.bootstrapping.beans.SentenceBean;
import data_deepprocessing.algorithm.bootstrapping.db.BootSentenceDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月26日 下午9:25:32 
* @version 1.0  
*/

public class BootstrappingSentenceService {
	
	private BootSentenceDB bootSentenceDB;
	

	public BootSentenceDB getBootSentenceDB() {
		return bootSentenceDB;
	}


	public void setBootSentenceDB(BootSentenceDB bootSentenceDB) {
		this.bootSentenceDB = bootSentenceDB;
	}




	public List<SentenceBean> doFindBootstrappingValidSentence(){
		return bootSentenceDB.doFindValidSentence();
	}

}

















