package data_deepprocessing.algorithm.bootstrapping.service;

import java.util.List;


import data_deepprocessing.algorithm.bootstrapping.beans.Pattern2BSBean;
import data_deepprocessing.algorithm.bootstrapping.db.BootPatternDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月26日 上午11:27:22 
* @version 1.0  
*/

public class BootstrappingPatternService {
	private BootPatternDB patternDB;
	
	public BootPatternDB getPatternDB() {
		return patternDB;
	}
	public void setPatternDB(BootPatternDB patternDB) {
		this.patternDB = patternDB;
	}
	
	public void doUpdateBootStrappingPatt(Pattern2BSBean pattern2bsBean){
		Pattern2BSBean pattern = patternDB.doFindPattern(pattern2bsBean);
		if(pattern!=null){
			pattern.setLeft_locate(pattern2bsBean.getLeft_locate()+","+pattern.getLeft_locate());
			pattern.setRight_locate(pattern2bsBean.getRight_locate()+","+pattern.getRight_locate());
			pattern.setSentence_id(pattern2bsBean.getSentence_id()+","+pattern.getSentence_id());
			pattern.setRefcount(pattern.getRefcount()+1);
			patternDB.doUpdatePatt(pattern);
		}else{
			patternDB.doInsertPattern(pattern2bsBean);
		}
	}

	
	public void  doUpdateBootStrappingPatternMatch(Pattern2BSBean pattern2bsBean){
		 patternDB.doUpdatePatternMatch(pattern2bsBean);
	}
	
	public List<Pattern2BSBean> doFindBootstrappingValidPattern(int limitCondition){
		return patternDB.doFindValidPattern(limitCondition);
	}
	
	
}















