package data_deepprocessing.algorithm.bootstrapping.service;

import java.util.List;

import data_deepprocessing.algorithm.bootstrapping.beans.Seed2BSBean;
import data_deepprocessing.algorithm.bootstrapping.db.BootSeedDB;
import data_deepprocessing.algorithm.bootstrapping.db.BootSymptomDB;
import data_deepprocessing.util.RegularExpressionUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月26日 上午11:27:22 
* @version 1.0  
*/

public class BootstrappingSeedService {
	private BootSeedDB bootSeedDB;
	private BootSymptomDB bootSymptomDB;
	
	public void doGenerateInitSeed(int limitFrequent){
		List<String> seedWords = bootSymptomDB.doFindUsefulSymptomWords();
		for(String seed:seedWords){
			Seed2BSBean bean = new Seed2BSBean();
			bean.setRefcount(limitFrequent);
			bean.setFreqcount(limitFrequent);
			bean.setMatched(0);
			bean.setSeed_start("");
			bean.setPattern_id("");
			bean.setSeed_content(RegularExpressionUtil.getRegularizedSentence(seed));
			bean.setSeed_class("init");
			bean.setSentence_id("");
			bean.setIs_check(0);
			bootSeedDB.doInsertSeed(bean);
		}
	}
	
	public void doUpdateBootStrappingSeed(Seed2BSBean seed2bsBean){
		Seed2BSBean seed = bootSeedDB.doFindSeed(seed2bsBean);
		if(seed != null){
			seed.setPattern_id(seed2bsBean.getPattern_id()+","+seed.getPattern_id());
			seed.setSeed_start(seed2bsBean.getSeed_start()+","+seed.getSeed_start());
			seed.setSentence_id(seed2bsBean.getSentence_id()+","+seed.getSentence_id());
			seed.setRefcount(seed.getRefcount()+1);
			bootSeedDB.doUpdateSeed(seed);
		}else{
			bootSeedDB.doInsertSeed(seed2bsBean);
		}
	}
	
	public void doUpdateBootStrappingSeedMatch(Seed2BSBean seed2bsBean){
		bootSeedDB.doUpdateSeedMatch(seed2bsBean);
	}
	
	public List<Seed2BSBean> doFindBootstrappingValidSeed(int limitCondition){
		return bootSeedDB.doFindValidSeed(limitCondition);
	}
	
	public BootSeedDB getBootSeedDB() {
		return bootSeedDB;
	}
	public void setBootSeedDB(BootSeedDB bootSeedDB) {
		this.bootSeedDB = bootSeedDB;
	}
	
	public BootSymptomDB getBootSymptomDB() {
		return bootSymptomDB;
	}
	public void setBootSymptomDB(BootSymptomDB bootSymptomDB) {
		this.bootSymptomDB = bootSymptomDB;
	}

}







