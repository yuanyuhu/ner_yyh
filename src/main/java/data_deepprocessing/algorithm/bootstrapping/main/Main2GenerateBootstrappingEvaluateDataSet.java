package data_deepprocessing.algorithm.bootstrapping.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.algorithm.bootstrapping.evaluate.GenerateEvaluateDataSetService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月15日 上午9:09:08 
* @version 1.0  
*/

public class Main2GenerateBootstrappingEvaluateDataSet {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		GenerateEvaluateDataSetService service = (GenerateEvaluateDataSetService) context.getBean("generateEvaluateDataSetService");
		
		service.generateStandardDataSet();
		 
	}
}
