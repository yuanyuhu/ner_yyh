package data_deepprocessing.algorithm.bootstrapping.db;

import java.util.List;

import data_deepprocessing.algorithm.bootstrapping.beans.Seed2BSBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月25日 上午9:40:35 
* @version 1.0  
*/

public interface BootSeedDB {
	
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月25日 上午9:45:56 
	* @parameter limitCondition 主要用来限制refcount:种子生成不同模式的个数
	* @return
	* @throws
	* 
	*/
	public List<Seed2BSBean> doFindValidSeed(int limitCondition);
	
	public List<String> doSelectBootstrappingSeedContent();
	
	public void doUpdateSeedMatch(Seed2BSBean seed2bsBean);
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月26日 上午11:37:56 
	* @parameter 
	* @return
	* @throws
	* 根据条件找到相应的种子
	*/
	public Seed2BSBean doFindSeed(Seed2BSBean seed2bsBean);
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月26日 上午11:38:37 
	* @parameter 
	* @return
	* @throws
	* 更新种子表
	*/
	public void doUpdateSeed(Seed2BSBean seed2bsBean);
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月26日 上午11:38:53 
	* @parameter 
	* @return
	* @throws
	* 插入种子表
	*/
	public void doInsertSeed(Seed2BSBean seed2bsBean);

}











