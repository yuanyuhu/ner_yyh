package data_deepprocessing.algorithm.bootstrapping.db;

import java.util.List;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月26日 下午8:53:40 
* @version 1.0  
*/

public interface BootSymptomDB {
	
	public List<String> doFindUsefulSymptomWords();

}
