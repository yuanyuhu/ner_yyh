package data_deepprocessing.algorithm.bootstrapping.db;

import java.util.List;

import data_deepprocessing.algorithm.bootstrapping.beans.Pattern2BSBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月25日 上午9:40:47 
* @version 1.0  
*/

public interface BootPatternDB {
	
	public List<Pattern2BSBean> doFindValidPattern(int limitCondition);
	
	public void doInsertPattern(Pattern2BSBean pattern2bsBean);
	
	public void doUpdatePatternMatch(Pattern2BSBean pattern2bsBean);
	
	public void doUpdatePatt(Pattern2BSBean pattern2bsBean);
	
	public Pattern2BSBean doFindPattern(Pattern2BSBean pattern2bsBean);
	
	
}














