package data_deepprocessing.algorithm.bootstrapping.db;

import java.util.List;

import data_deepprocessing.algorithm.bootstrapping.beans.SentenceBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月26日 下午9:28:43 
* @version 1.0  
*/

public interface BootSentenceDB {
	
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月27日 上午8:48:17 
	* @parameter 
	* @return
	* @throws
	* 这里目前只是为了试验用，后期还是要改的，目前最好的是
	* sentence_id,content的这种格式
	*/
	public List<SentenceBean> doFindValidSentence();

}
