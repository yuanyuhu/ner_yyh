package data_deepprocessing.algorithm.bootstrapping.beans;


/**
 * @author YuanYuhu
 *
 */
public class Seed2BSBean {
	
	private int seed_id;				//种子id
	private String seed_content;		//种子内容
	private int matched;				//种子是否参与匹配
	private int freqcount;				//该种子产生的次数
	private int refcount;				//种子产生不同模式的个数
	private String seed_start;			//种子起始位置
	private String sentence_id;			//句子id
	private String pattern_id;			//使用到的模式的id
	private String seed_class;			//种子类别init/bootstrapping/crfs
	private int is_check;				//是否经过检验
	
	
	public int getSeed_id() {
		return seed_id;
	}
	public void setSeed_id(int seed_id) {
		this.seed_id = seed_id;
	}
	public String getSeed_content() {
		return seed_content;
	}
	public void setSeed_content(String seed_content) {
		this.seed_content = seed_content;
	}
	public int getMatched() {
		return matched;
	}
	public void setMatched(int matched) {
		this.matched = matched;
	}
	public int getFreqcount() {
		return freqcount;
	}
	public void setFreqcount(int freqcount) {
		this.freqcount = freqcount;
	}
	public int getRefcount() {
		return refcount;
	}
	public void setRefcount(int refcount) {
		this.refcount = refcount;
	}
	public String getSeed_start() {
		return seed_start;
	}
	public void setSeed_start(String seed_start) {
		this.seed_start = seed_start;
	}
	public String getSentence_id() {
		return sentence_id;
	}
	public void setSentence_id(String sentence_id) {
		this.sentence_id = sentence_id;
	}
	public String getPattern_id() {
		return pattern_id;
	}
	public void setPattern_id(String pattern_id) {
		this.pattern_id = pattern_id;
	}
	
	public String getSeed_class() {
		return seed_class;
	}
	public void setSeed_class(String seed_class) {
		this.seed_class = seed_class;
	}
	
	public int getIs_check() {
		return is_check;
	}
	public void setIs_check(int is_check) {
		this.is_check = is_check;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((seed_content == null) ? 0 : seed_content.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seed2BSBean other = (Seed2BSBean) obj;
		if (seed_content == null) {
			if (other.seed_content != null)
				return false;
		} else if (!seed_content.equals(other.seed_content))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Seed2BSBean [seed_id=" + seed_id + ", seed_content=" + seed_content + ", matched=" + matched
				+ ", freqcount=" + freqcount + ", refcount=" + refcount + ", seed_start=" + seed_start
				+ ", sentence_id=" + sentence_id + ", pattern_id=" + pattern_id + ", seed_class=" + seed_class
				+ ", is_check=" + is_check + "]";
	}
	

	
	
}
