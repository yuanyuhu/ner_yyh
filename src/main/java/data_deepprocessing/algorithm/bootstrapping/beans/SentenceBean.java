package data_deepprocessing.algorithm.bootstrapping.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月28日 上午9:47:29 
* @version 1.0  
*/

public class SentenceBean {
	
	private int id;
	private String sentence;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSentence() {
		return sentence;
	}
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((sentence == null) ? 0 : sentence.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SentenceBean other = (SentenceBean) obj;
		if (id != other.id)
			return false;
		if (sentence == null) {
			if (other.sentence != null)
				return false;
		} else if (!sentence.equals(other.sentence))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SentenceBean [id=" + id + ", sentence=" + sentence + "]";
	}
	
	

}
