package data_deepprocessing.algorithm.bootstrapping.beans;


/**
 * @author YuanYuhu
 *
 */
public class Pattern2BSBean {
	
	private int pattern_id;			//模式的ID
	private String left_pat;		//模式左边字符串
	private String right_pat;		//模式右边字符串
	private String left_locate;		//左边的位置
	private String right_locate;	//右边的位置
	private int freqcount;			//该模式产生的次数
	private int refcount;		    //模式产生的不同的种子数量
	private int matched;			//模式是否已参与种子抽取
	private String sentence_id;		//句子ID	
	
	
	public int getPattern_id() {
		return pattern_id;
	}
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}
	public String getLeft_pat() {
		return left_pat;
	}
	public void setLeft_pat(String left_pat) {
		this.left_pat = left_pat;
	}
	public String getRight_pat() {
		return right_pat;
	}
	public void setRight_pat(String right_pat) {
		this.right_pat = right_pat;
	}
	
	public int getFreqcount() {
		return freqcount;
	}
	public void setFreqcount(int freqcount) {
		this.freqcount = freqcount;
	}
	public int getRefcount() {
		return refcount;
	}
	public void setRefcount(int refcount) {
		this.refcount = refcount;
	}
	public int getMatched() {
		return matched;
	}
	public void setMatched(int matched) {
		this.matched = matched;
	}
	
	public String getSentence_id() {
		return sentence_id;
	}
	public void setSentence_id(String sentence_id) {
		this.sentence_id = sentence_id;
	}
	
	public String getLeft_locate() {
		return left_locate;
	}
	public void setLeft_locate(String left_locate) {
		this.left_locate = left_locate;
	}
	public String getRight_locate() {
		return right_locate;
	}
	public void setRight_locate(String right_locate) {
		this.right_locate = right_locate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((left_pat == null) ? 0 : left_pat.hashCode());
		result = prime * result + ((right_pat == null) ? 0 : right_pat.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pattern2BSBean other = (Pattern2BSBean) obj;
		if (left_pat == null) {
			if (other.left_pat != null)
				return false;
		} else if (!left_pat.equals(other.left_pat))
			return false;
		if (right_pat == null) {
			if (other.right_pat != null)
				return false;
		} else if (!right_pat.equals(other.right_pat))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Pattern2BSBean [pattern_id=" + pattern_id + ", left_pat=" + left_pat + ", right_pat=" + right_pat
				+ ", left_locate=" + left_locate + ", right_locate=" + right_locate + ", freqcount=" + freqcount
				+ ", refcount=" + refcount + ", matched=" + matched + ", sentence_id=" + sentence_id + "]";
	}
	
	
		     
}
