package data_deepprocessing.prepareData.main;

import java.io.IOException;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.beans.Word_Embedding_xbs2vecBean;
import data_deepprocessing.prepareData.beans.YYH_XianBingShiBean;
import data_deepprocessing.prepareData.db.Word_Embedding_init_seedDB;
import data_deepprocessing.prepareData.db.Word_Embedding_xbs2vecDB;
import data_deepprocessing.prepareData.db.YYH_XianBingShiDB;
import data_deepprocessing.prepareData.service.Word_Embedding_xbs2vecService;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月5日 下午4:23:26 
* @version 1.0  
*/

public class Main2WordEmbeddingSegment {

	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Word_Embedding_xbs2vecService service = (Word_Embedding_xbs2vecService) context.getBean("word_Embedding_xbs2vecService");
		
		try {
			service.generateSegmentXianBingshiByInitSeed();
			service.writeSegmented2LocalFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}










