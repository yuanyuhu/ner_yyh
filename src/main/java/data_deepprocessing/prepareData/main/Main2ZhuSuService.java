package data_deepprocessing.prepareData.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.ExtractXianBingShiService;
import data_deepprocessing.prepareData.service.ExtractZhuSuService;
import data_deepprocessing.prepareData.service.SegmentXianBingShiService;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年2月19日 上午9:41:42 
* @version 1.0  
*/

public class Main2ZhuSuService {
	
	
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月28日 上午9:32:56 
	* @parameter 
	* @return
	* @throws
	* 抽取现病史
	*/
//	public static void main(String[] args){
//		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//		
//		ExtractXianBingShiService service = (ExtractXianBingShiService) context.getBean("extractXianBingShiService");
//		
//		service.saveXianBingShi2Oracle();
//	}
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月28日 上午9:33:15 
	* @parameter 
	* @return
	* @throws
	* 抽取主诉
	*/
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		ExtractZhuSuService service = (ExtractZhuSuService) context.getBean("extractZhuSuService");
		
		service.saveZhuSu2Oracle();
	}

}


















