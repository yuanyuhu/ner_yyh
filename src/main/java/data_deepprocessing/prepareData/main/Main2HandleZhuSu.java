package data_deepprocessing.prepareData.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.HandleZhuSu_NewService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月8日 上午10:20:03 
* @version 1.0  
*/

public class Main2HandleZhuSu {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		HandleZhuSu_NewService service = (HandleZhuSu_NewService) context.getBean("handleZhuSu_NewService");
		
//		service.handleZhuSu();
		List<String> list = service.getHandledZhuSu();
				for(String string : list){
			System.out.println(string);
		}
//		List<XianBingShi_New_Bean> beans = service.getXianBingShi_New();
//		for(XianBingShi_New_Bean bean : beans){
//			System.out.println(bean);
//		}

		
	}

}
