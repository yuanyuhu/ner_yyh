package data_deepprocessing.prepareData.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.Word_Embedding_EN_xbs2vecService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月14日 上午9:27:40 
* @version 1.0  
*/

public class Main2WordEmbeddingVector2En {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Word_Embedding_EN_xbs2vecService service = (Word_Embedding_EN_xbs2vecService) context.getBean("word_Embedding_EN_xbs2vecService");
		
		try {
			service.writeXianBingShiEn2LocalFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
