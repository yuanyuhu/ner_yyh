package data_deepprocessing.prepareData.main;


import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.Word_Embedding_vectorService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午1:14:40 
* @version 1.0  
*/

public class Mian2SegmentService {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Word_Embedding_vectorService service = (Word_Embedding_vectorService) context.getBean("word_Embedding_vectorService");
		
//		service.generateSegmentXianBingshiBySymptom();
		try {
			service.word_embedding_vector2Oracle();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
