package data_deepprocessing.prepareData.main;


import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.HandleSeedWordService;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午9:17:57 
* @version 1.0  
*/

public class Main2HandleSeedWord {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		HandleSeedWordService service = (HandleSeedWordService) context.getBean("handleSeedWordService");
		
		List<String> list = service.getHandle_SeedWord();
		for(String string : list){
			System.out.println(string);
		}
		
	}

}
