package data_deepprocessing.prepareData.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.GenerateGraph2GephiService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月14日 上午9:27:40 
* @version 1.0  
*/

public class Main2Gephi {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		GenerateGraph2GephiService service = (GenerateGraph2GephiService) context.getBean("generateGraph2GephiService");
		
		service.function();
	}

}
