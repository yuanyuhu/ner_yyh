package data_deepprocessing.prepareData.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.HandleInitSeed2EnService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月14日 上午9:27:40 
* @version 1.0  
*/

public class Main2InitSeed2En {
	
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		HandleInitSeed2EnService service = (HandleInitSeed2EnService) context.getBean("handleInitSeed2EnService");
		
		service.filterDuplicateValue();
	}

}
