package data_deepprocessing.prepareData.main;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.SegmentXianBingShiService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午10:23:03 
* @version 1.0  
*/

public class Main2WordEmbeddingVector2Oracle {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		SegmentXianBingShiService service = (SegmentXianBingShiService) context.getBean("segmentXianBingShiService");
		
//		service.generateSegmentXianBingshiBySymptom();
		try {
			service.writeContent_Segmented2LocalFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
