package data_deepprocessing.prepareData.main;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.service.SegmentXianBingShiService;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 下午10:08:46 
* @version 1.0  
*/

public class Main2SegmentZhang {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		SegmentXianBingShiService service = (SegmentXianBingShiService) context.getBean("segmentXianBingShiService");
		
		service.generateSegmentXianBingshiByInitSeed_Zhang();
	}

}
