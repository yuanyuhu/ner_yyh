package data_deepprocessing.prepareData.service;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 上午10:09:33 
* @version 1.0  
*/

public class TestService {
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}
	
	public List<XianBingShi_new_zhangBean> getList(){
		return xianBingShi_New_zhangDB.selectALlXianBingShi();
	}
	
	
	public static void main(String[] args){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		TestService service = (TestService) context.getBean("testService");
		
		List<XianBingShi_new_zhangBean> list = service.getList();
		for(XianBingShi_new_zhangBean bean : list){
			System.out.println(bean);
		}
		
	}
	

}














