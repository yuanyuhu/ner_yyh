package data_deepprocessing.prepareData.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import data_deepprocessing.prepareData.beans.InitSeedBean;
import data_deepprocessing.prepareData.db.InitSeedDB;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月27日 下午8:53:57 
* @version 1.0  
*/

public class HandleInitSeed2EnService {
	
	private InitSeedDB initSeedDB;

	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}
	
	private List<InitSeedBean> getInitSeedBean2En(){
		return initSeedDB.selectAllInitSeedBean2En();
	}
	
	
	public void filterDuplicateValue(){
		List<InitSeedBean> initSeedBeans = getInitSeedBean2En();
		Set<String> seedSet  = new HashSet<>();
		for(InitSeedBean bean : initSeedBeans){
			String seed = bean.getContent();
			if(seed.contains(", ")){
				String[] seeds = seed.split(", ");
				for(String s: seeds){
					seedSet.add(s.toLowerCase());
				}
			}else{
				seedSet.add(seed.toLowerCase());
			}
		}
		List<InitSeedBean> initSeedBeanList = new ArrayList<>();
		
		for(String seed:seedSet){
			InitSeedBean bean = new InitSeedBean();
			bean.setContent(seed);
			bean.setTag("");
			initSeedBeanList.add(bean);
		}
		for(InitSeedBean bean : initSeedBeanList){
			initSeedDB.insertXianBingShi2En(bean);
		}
	}
	
	
	
	
	
	
	
	
	

}
