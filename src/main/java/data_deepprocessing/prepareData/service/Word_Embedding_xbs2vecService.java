package data_deepprocessing.prepareData.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import data_deepprocessing.prepareData.beans.Word_Embedding_xbs2vecBean;
import data_deepprocessing.prepareData.db.Word_Embedding_xbs2vecDB;
import data_deepprocessing.prepareData.db.Word_Embedding_init_seedDB;
import data_deepprocessing.util.FileUtil;
import data_deepprocessing.util.NlpirSegmentUtil.CLibrary;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月5日 下午3:56:20 
* @version 1.0  
*/

public class Word_Embedding_xbs2vecService {
	public static Logger logger = Logger.getLogger(Word_Embedding_xbs2vecService.class.getName());
	
	private Word_Embedding_xbs2vecDB word_Embedding_xbs2vecDB;
	private Word_Embedding_init_seedDB word_Embedding_init_seedDB;
	
	public void generateSegmentXianBingshiByInitSeed(){
		String argu = System.getProperty("user.dir");
		int charset_type = 1;
		int init_flag = CLibrary.Instance.NLPIR_Init(argu, charset_type, "0");
		String nativeBytes = null;
		if (0 == init_flag) {
			nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			logger.info("初始化失败！fail reason is "+nativeBytes);
		}
		try {
			for(String initSeed : word_Embedding_init_seedDB.selectInitSeed2WordEmbedding()){
				CLibrary.Instance.NLPIR_AddUserWord(initSeed);
			}
			for(Word_Embedding_xbs2vecBean word_embedding_xbs2vecBean: word_Embedding_xbs2vecDB.selectAllXBS()){
				String sInput = word_embedding_xbs2vecBean.getXianbingshi();
				nativeBytes = CLibrary.Instance.NLPIR_ParagraphProcess(sInput, 1);
				Logger.getLogger("yuan_data").info("增加用户词典后分词结果为:" + nativeBytes);
				word_embedding_xbs2vecBean.setSegmented(nativeBytes.replaceAll("(/[a-z,0-9]+ )", " "));
				word_Embedding_xbs2vecDB.updateXBS(word_embedding_xbs2vecBean);
			}
			CLibrary.Instance.NLPIR_Exit();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
	}
	
	public void writeSegmented2LocalFile() throws IOException{
		String path = System.getProperty("user.dir");
		BufferedWriter writer = FileUtil.getWriter(path+File.separator+"temp"+File.separator+"segmented_xianbingshi.txt");
		List<String> content_segmenteds = word_Embedding_xbs2vecDB.selectSegmented();
		for(String content: content_segmenteds){
			if(content==null && content.equals("")){
				continue;
			}
			writer.write(content);
			writer.flush();
			writer.newLine();
		}
		if(writer!=null){
			writer.close();
		}
	}
	
	
	
	
	
	public Word_Embedding_xbs2vecDB getWord_Embedding_xbs2vecDB() {
		return word_Embedding_xbs2vecDB;
	}
	public void setWord_Embedding_xbs2vecDB(Word_Embedding_xbs2vecDB word_Embedding_xbs2vecDB) {
		this.word_Embedding_xbs2vecDB = word_Embedding_xbs2vecDB;
	}

	public Word_Embedding_init_seedDB getWord_Embedding_init_seedDB() {
		return word_Embedding_init_seedDB;
	}

	public void setWord_Embedding_init_seedDB(Word_Embedding_init_seedDB word_Embedding_init_seedDB) {
		this.word_Embedding_init_seedDB = word_Embedding_init_seedDB;
	}
	
}
