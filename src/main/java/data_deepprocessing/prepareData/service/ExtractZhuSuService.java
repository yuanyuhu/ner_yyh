package data_deepprocessing.prepareData.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data_deepprocessing.prepareData.beans.Text_CaseBean;
import data_deepprocessing.prepareData.beans.YYH_ZhuSuBean;
import data_deepprocessing.prepareData.db.Text_CaseDB;
import data_deepprocessing.prepareData.db.YYH_ZhuSuDB;
import data_deepprocessing.util.RegularExpressionUtil;

/**
 * @author 作者 : YUHU YUAN
 * @date 创建时间：2017年2月19日 上午9:06:22
 * @version 1.0
 */

public class ExtractZhuSuService {
	private Text_CaseDB text_CaseDB;
	private YYH_ZhuSuDB yyh_ZhuSuDB;

	public Text_CaseDB getText_CaseDB() {
		return text_CaseDB;
	}

	public void setText_CaseDB(Text_CaseDB text_CaseDB) {
		this.text_CaseDB = text_CaseDB;
	}

	public YYH_ZhuSuDB getYyh_ZhuSuDB() {
		return yyh_ZhuSuDB;
	}

	public void setYyh_ZhuSuDB(YYH_ZhuSuDB yyh_ZhuSuDB) {
		this.yyh_ZhuSuDB = yyh_ZhuSuDB;
	}

	private String extractZhuSu(String textCase) {
		Pattern pattern = Pattern.compile("主诉：(.+)\\r");
		Matcher matcher = pattern.matcher(textCase);
		while (matcher.find()) {
			String zhusu = matcher.group(1);
			if (!"".equals(zhusu)) {
				return zhusu;
			} else {
				return "";
			}
		}
		return "";
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月19日 上午9:39:45
	 * @parameter
	 * @return
	 * @throws 存到list中，这个功能貌似现在有点多余的样子
	 */
	public List<YYH_ZhuSuBean> saveZhuSu2List() {
		List<YYH_ZhuSuBean> zhuSuBeans = new ArrayList<>();
		List<Text_CaseBean> text_CaseBeans = text_CaseDB.selectZhuSuInfo();
		for (Text_CaseBean text_CaseBean : text_CaseBeans) {
			String content = text_CaseBean.getTxt_info();
			if (!"".equals(content)) {
				String zhusu = extractZhuSu(content);
				zhuSuBeans.add(new YYH_ZhuSuBean(text_CaseBean.getIncase_id(), zhusu,
						RegularExpressionUtil.getRegularizedSentence(zhusu)));
			}
		}
		return zhuSuBeans;
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月19日 上午9:40:13
	 * @parameter
	 * @return
	 * @throws 插入数据库，这里可以尝试批量插入提升性能，不过现在只有1000+的数据，对时间等各种不是要求很高
	 */
	public void saveZhuSu2Oracle() {
		List<Text_CaseBean> text_CaseBeans = text_CaseDB.selectZhuSuInfo();
		for (Text_CaseBean text_CaseBean : text_CaseBeans) {
			String content = text_CaseBean.getTxt_info();
			if (!"".equals(content)) {
				String zhusu = extractZhuSu(content);
				yyh_ZhuSuDB.insertZhuShu(new YYH_ZhuSuBean(text_CaseBean.getIncase_id(), zhusu,
						RegularExpressionUtil.getRegularizedSentence(zhusu)));
			}
		}
	}

}
