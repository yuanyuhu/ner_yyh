package data_deepprocessing.prepareData.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.bytedeco.javacpp.videoInputLib.videoDevice;

import com.google.inject.spi.PrivateElements;

import data_deepprocessing.prepareData.beans.InitSeedBean;
import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.beans.YYH_XianBingShiBean;
import data_deepprocessing.prepareData.db.InitSeedDB;
import data_deepprocessing.prepareData.db.SymptomDB;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;
import data_deepprocessing.prepareData.db.YYH_XianBingShiDB;
import data_deepprocessing.util.FileUtil;
import data_deepprocessing.util.NlpirSegmentUtil.CLibrary;
import scala.xml.dtd.PublicID;
import data_deepprocessing.util.RegularExpressionUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:25:32 
* @version 1.0  
*/

public class SegmentXianBingShiService {
	public static Logger logger = Logger.getLogger(SegmentXianBingShiService.class.getName());
	
	private YYH_XianBingShiDB yyh_XianBingShiDB;
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	private InitSeedDB initSeedDB;
	private SymptomDB symptomDB;
	
	public List<InitSeedBean> getXianBingShi(){
		return initSeedDB.selectAllInitSeed();
	}
	
	public void generateSegmentXianBingshiByInitSeed(){
		String argu = System.getProperty("user.dir");
		int charset_type = 1;
		int init_flag = CLibrary.Instance.NLPIR_Init(argu, charset_type, "0");
		String nativeBytes = null;
		if (0 == init_flag) {
			nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			logger.info("初始化失败！fail reason is "+nativeBytes);
		}
		try {
			for(InitSeedBean initSeedBean : initSeedDB.selectAllInitSeed()){
				CLibrary.Instance.NLPIR_AddUserWord(initSeedBean.getContent());
			}
			for(YYH_XianBingShiBean yyh_XianBingShiBean : yyh_XianBingShiDB.selectAllXianBingShi()){
				String sInput = yyh_XianBingShiBean.getContent_handled();
				nativeBytes = CLibrary.Instance.NLPIR_ParagraphProcess(sInput, 1);
				Logger.getLogger("yuan_data").info("增加用户词典后分词结果为:" + nativeBytes);
				yyh_XianBingShiBean.setContent_segmented(nativeBytes.replaceAll("(/[a-z,0-9]+ )", " "));
				yyh_XianBingShiDB.updateXianBingshiForSegment(yyh_XianBingShiBean);
			}
			CLibrary.Instance.NLPIR_Exit();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
	}
	
	public void generateSegmentXianBingshiBySymptom(){
		String argu = System.getProperty("user.dir");
		int charset_type = 1;
		int init_flag = CLibrary.Instance.NLPIR_Init(argu, charset_type, "0");
		String nativeBytes = null;
		if (0 == init_flag) {
			nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			logger.info("初始化失败！fail reason is "+nativeBytes);
		}
		try {
			for(String word : symptomDB.selectAllDistinctSymptomContent()){
				CLibrary.Instance.NLPIR_AddUserWord(RegularExpressionUtil.getRegularizedSentence(word));
			}
			for(YYH_XianBingShiBean yyh_XianBingShiBean : yyh_XianBingShiDB.selectAllXianBingShi()){
				String sInput = yyh_XianBingShiBean.getContent_handled();
				nativeBytes = CLibrary.Instance.NLPIR_ParagraphProcess(sInput, 1);
				Logger.getLogger("yuan_data").info("增加用户词典后分词结果为:" + nativeBytes);
				yyh_XianBingShiBean.setContent_segmented(nativeBytes.replaceAll("(/[a-z,0-9]+ )", " "));
				yyh_XianBingShiDB.updateXianBingshiForSegment(yyh_XianBingShiBean);
			}
			CLibrary.Instance.NLPIR_Exit();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	public void generateSegmentXianBingshiByInitSeed_Zhang(){
		String argu = System.getProperty("user.dir");
		int charset_type = 1;
		int init_flag = CLibrary.Instance.NLPIR_Init(argu, charset_type, "0");
		String nativeBytes = null;
		if (0 == init_flag) {
			nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			logger.info("初始化失败！fail reason is "+nativeBytes);
		}
		try {
			for(String word : initSeedDB.selectAllSeedContent()){
				CLibrary.Instance.NLPIR_AddUserWord(RegularExpressionUtil.getRegularizedSentence(word));
			}
			for(XianBingShi_new_zhangBean bean : xianBingShi_New_zhangDB.selectALlXianBingShi()){
				String sInput = bean.getSubxianbingshi();
				nativeBytes = CLibrary.Instance.NLPIR_ParagraphProcess(sInput, 1);
				Logger.getLogger("yuan_data").info("增加用户词典后分词结果为:" + nativeBytes);
				bean.setContent_segmented(nativeBytes.replaceAll("(/[a-z,0-9]+ )", " "));
				xianBingShi_New_zhangDB.update2AddContent_Segmented(bean);
			}
			CLibrary.Instance.NLPIR_Exit();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	
	
	public void writeContent_Segmented2LocalFile() throws IOException{
		String path = System.getProperty("user.dir");
		BufferedWriter writer = FileUtil.getWriter(path+File.separator+"temp"+File.separator+"segmented_xianbingshi.txt");
		List<String> content_segmenteds = yyh_XianBingShiDB.selectXBSContent_Segmented();
		for(String content: content_segmenteds){
			writer.write(content);
			writer.flush();
			writer.newLine();
		}
		if(writer!=null){
			writer.close();
		}
	}

	public YYH_XianBingShiDB getYyh_XianBingShiDB() {
		return yyh_XianBingShiDB;
	}

	public void setYyh_XianBingShiDB(YYH_XianBingShiDB yyh_XianBingShiDB) {
		this.yyh_XianBingShiDB = yyh_XianBingShiDB;
	}
	
	public InitSeedDB getInitSeedDB() {
		return initSeedDB;
	}

	public void setInitSeedDB(InitSeedDB initSeedDB) {
		this.initSeedDB = initSeedDB;
	}

	public SymptomDB getSymptomDB() {
		return symptomDB;
	}

	public void setSymptomDB(SymptomDB symptomDB) {
		this.symptomDB = symptomDB;
	}

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}
	
	
	
}

















