package data_deepprocessing.prepareData.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data_deepprocessing.prepareData.beans.Text_CaseBean;
import data_deepprocessing.prepareData.beans.YYH_XianBingShiBean;
import data_deepprocessing.prepareData.beans.YYH_ZhuSuBean;
import data_deepprocessing.prepareData.db.Text_CaseDB;
import data_deepprocessing.prepareData.db.YYH_XianBingShiDB;
import data_deepprocessing.util.RegularExpressionUtil;

/**
 * @author 作者 : YUHU YUAN
 * @date 创建时间：2017年2月19日 上午9:06:22
 * @version 1.0
 */

public class ExtractXianBingShiService {
	private Text_CaseDB text_CaseDB;
	private YYH_XianBingShiDB yyh_XianBingShiDB;

	public Text_CaseDB getText_CaseDB() {
		return text_CaseDB;
	}

	public void setText_CaseDB(Text_CaseDB text_CaseDB) {
		this.text_CaseDB = text_CaseDB;
	}

	public YYH_XianBingShiDB getYyh_XianBingShiDB() {
		return yyh_XianBingShiDB;
	}

	public void setYyh_XianBingShiDB(YYH_XianBingShiDB yyh_XianBingShiDB) {
		this.yyh_XianBingShiDB = yyh_XianBingShiDB;
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月19日 下午10:47:23
	 * @parameter
	 * @return
	 * @throws 
	 * 现病史：~~~~~。这种正常情况
	 * 
	 */
	private String extractXianBingShiPatternOne(String textCase) {
		Pattern pattern = Pattern.compile("现病史[：:]*(.+)\r");
		Matcher matcher = pattern.matcher(textCase);
		while (matcher.find()) {
			System.out.println("count" + matcher.groupCount());

			String xianbingshi = matcher.group(1);
			if (!"".equals(xianbingshi)) {
				return xianbingshi;
			} else {
				return "";
			}
		}
		return "";
	}
	
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年2月20日 上午11:35:13 
	* @parameter 
	* @return
	* @throws
	*  现病史，四诊，现症等情况；
	*/
	private String extractXianBingShiPatternTwo(String textCase) {
		String xianbingshi= extractXianBingShiPatternOne(textCase);
		if("".equals(xianbingshi)){
			String[] tempStrings = textCase.split("\r");
			if(!textCase.contains("现症")){
				for(int i=0; i<tempStrings.length; ++i){
					String string = tempStrings[i];
					Pattern pattern = Pattern.compile("([一二三四五六七八]+诊)+");
					Matcher matcher = pattern.matcher(string);
					int groupCount = 0;
					while (matcher.find()) {
						groupCount = matcher.groupCount();
					}
					if(groupCount!=0){
						return string.trim();
					}
					
				}
			}else{
				StringBuffer result = new StringBuffer();
				for(int i=0; i<tempStrings.length; ++i){
					String string = tempStrings[i];
					Pattern pattern = Pattern.compile("([一二三四五六七八]+诊)+");
					Matcher matcher = pattern.matcher(string);
					int groupCount = 0;
					while (matcher.find()) {
						groupCount = matcher.groupCount();
					}
					if(groupCount!=0){
						result.append(string);
						break;
					}
				}
				Pattern pattern = Pattern.compile("现症[：:]*(.+)\r");
				Matcher matcher = pattern.matcher(textCase);
				while (matcher.find()) {
					String xianzheng = matcher.group(1);
					if (!"".equals(xianzheng)) {
						result.append(xianzheng);
					} else {
						result.append("");
					}
				}
				return result.toString().trim();
				
			}
			
		}else{
			return xianbingshi.trim();
		}
		return "";
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 上午8:21:28
	 * @parameter
	 * @return
	 * @throws e.g.
	 *             舌脉诊：舌质暗 ，舌体胖 ，舌苔腻略黄 ，脉象弦细数 。
	 */
	private String extractSheMaiZhenPatternOne(String textCase) {
		Pattern pattern = Pattern.compile("舌脉诊[：:]*(.+)");
		Matcher matcher = pattern.matcher(textCase);
		while (matcher.find()) {

			String shemaizhen = matcher.group(1);
			if (!"".equals(shemaizhen)) {
				return shemaizhen;
			} else {
				return "";
			}
		}
		return "";
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月20日 上午8:21:23
	 * @parameter
	 * @return
	 * @throws 
	 * 舌脉诊：
	 *             舌质暗 ，舌体胖 ，舌苔腻略黄 ，脉象弦细数 。
	 * 如果没有舌脉诊的话就逐行扫描，直到能拿到为止。        
	 */
	private String extractSheMaiZhenPatternTwo(String textCase) {
		String shemaiString = extractSheMaiZhenPatternOne(textCase);
		if ("".equals(shemaiString)) {
			if (textCase.contains("舌脉诊")) {
				String[] tempStrings = textCase.split("\r");
				for (int i = 0; i < tempStrings.length; ++i) {
					String string = tempStrings[i];
					if (string.contains("舌脉诊")) {
						string = tempStrings[i + 1];
						if (string.contains("舌") || string.contains("脉")) {
							if (string.contains(":") || string.contains("克")) {
								return "";
							} else {
								return string.trim();
							}
						} else {
							return "";
						}
					}
				}
			} else {
				String[] tempStrings = textCase.split("\r");
				for (int i = 0; i < tempStrings.length; ++i) {
					String string = tempStrings[i];
					if (string.contains("舌") || string.contains("脉")) {
						if (string.contains("：") || string.contains("克") || string.contains(":")
								|| string.contains("g")||string.contains("年")) {
							continue;
						} else {
							return string.trim();
						}
					}
				}

			}
		}
		return shemaiString.trim();
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月19日 上午9:39:45
	 * @parameter
	 * @return
	 * @throws 存到list中，这个功能貌似现在有点多余的样子
	 */
	public List<YYH_ZhuSuBean> saveXianBingShi2List() {
		List<YYH_ZhuSuBean> zhuSuBeans = new ArrayList<>();
		List<Text_CaseBean> text_CaseBeans = text_CaseDB.selectAllTxtInfo();
		for (Text_CaseBean text_CaseBean : text_CaseBeans) {
			String content = text_CaseBean.getTxt_info();
			if (!"".equals(content)) {
				// zhuSuBeans.add(new
				// YYH_ZhuSuBean(text_CaseBean.getIncase_id(),
				// extractXianBingShi(content)));
				System.out.println(text_CaseBean.getIncase_id() + "+++==" + extractXianBingShiPatternTwo(content));

			}
		}
		return zhuSuBeans;
	}

	/**
	 * @author 作者 : YUHU YUAN
	 * @date 创建时间：2017年2月19日 上午9:40:13
	 * @parameter
	 * @return
	 * @throws 插入数据库，这里可以尝试批量插入提升性能，不过现在只有1000+的数据，对时间等各种不是要求很高
	 */
	public void saveXianBingShi2Oracle() {
		List<Text_CaseBean> text_CaseBeans = text_CaseDB.selectAllTxtInfo();
		for (Text_CaseBean text_CaseBean : text_CaseBeans) {
			String content = text_CaseBean.getTxt_info();
			if (!"".equals(content)) {
				String xianbingshi = extractXianBingShiPatternTwo(content);
				if(content.contains("主诉")){
					yyh_XianBingShiDB.insertXianBingShi(new YYH_XianBingShiBean(text_CaseBean.getIncase_id(),
							 xianbingshi,RegularExpressionUtil.getRegularizedSentence(xianbingshi),"zhusu","现病史",""));
					yyh_XianBingShiDB.insertXianBingShi(new YYH_XianBingShiBean(text_CaseBean.getIncase_id(),
							xianbingshi,RegularExpressionUtil.getRegularizedSentence(xianbingshi),"zhusu","舌脉",""));
				}else{
					yyh_XianBingShiDB.insertXianBingShi(new YYH_XianBingShiBean(text_CaseBean.getIncase_id(),
							xianbingshi,RegularExpressionUtil.getRegularizedSentence(xianbingshi),"","现病史",""));
					yyh_XianBingShiDB.insertXianBingShi(new YYH_XianBingShiBean(text_CaseBean.getIncase_id(),
							xianbingshi,RegularExpressionUtil.getRegularizedSentence(xianbingshi),"","舌脉",""));
				}
			}
		}
	}

}















