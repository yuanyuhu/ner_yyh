package data_deepprocessing.prepareData.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;
import data_deepprocessing.prepareData.db.XianBingShi_New_zhangDB;
import data_deepprocessing.util.FilePathUtil;
import data_deepprocessing.util.FileUtil;
import data_deepprocessing.util.RegularExpressionUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月5日 下午3:56:20 
* @version 1.0  
*/

public class Word_Embedding_EN_xbs2vecService {
	public static Logger logger = Logger.getLogger(Word_Embedding_EN_xbs2vecService.class.getName());
	
	private XianBingShi_New_zhangDB xianBingShi_New_zhangDB;
	
	private List<XianBingShi_new_zhangBean> doGetXianBingShi2En(){
		return xianBingShi_New_zhangDB.selectALlXianBingShi2En();
	}
	
	public void writeXianBingShiEn2LocalFile() throws IOException{
		BufferedWriter writer = FileUtil.getWriter(FilePathUtil.SEGMENTED_XIANBINGSHI_EN_PATH);
		List<XianBingShi_new_zhangBean> xianbingshiBeans = doGetXianBingShi2En();
		for(XianBingShi_new_zhangBean bean: xianbingshiBeans){
			String content = bean.getSubxianbingshi().toLowerCase();
			content = RegularExpressionUtil.HandlePunctuation2EnSentence(content);
			if(content==null || content.equals("")){
				continue;
			}
			writer.write(content);
			writer.flush();
			writer.newLine();
		}
		if(writer!=null){
			writer.close();
		}
	}

	public XianBingShi_New_zhangDB getXianBingShi_New_zhangDB() {
		return xianBingShi_New_zhangDB;
	}

	public void setXianBingShi_New_zhangDB(XianBingShi_New_zhangDB xianBingShi_New_zhangDB) {
		this.xianBingShi_New_zhangDB = xianBingShi_New_zhangDB;
	}
	
	
}
