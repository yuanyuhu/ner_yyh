package data_deepprocessing.prepareData.service;

import java.util.List;

import data_deepprocessing.prepareData.beans.SeedWord2StructureDataSetBean;
import data_deepprocessing.prepareData.db.SeedWordDB;
import data_deepprocessing.util.RegularExpressionUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午9:13:27 
* @version 1.0  
*/

public class HandleSeedWordService {
	private SeedWordDB seedWordDB;

	public SeedWordDB getSeedWordDB() {
		return seedWordDB;
	}

	public void setSeedWordDB(SeedWordDB seedWordDB) {
		this.seedWordDB = seedWordDB;
	}
	
	public List<SeedWord2StructureDataSetBean> getSeedWordBeans(){
		return seedWordDB.selectAllSeedWord();
	}
	
	public void handleSeedWord(){
		List<SeedWord2StructureDataSetBean> beans = getSeedWordBeans();
		for(SeedWord2StructureDataSetBean bean : beans){
			bean.setHandle_seedword(RegularExpressionUtil.getRegularizedSentence(bean.getSeedword()));
			seedWordDB.updateSeedWord(bean);
		}
	}
	
	public List<String> getHandle_SeedWord(){
		return seedWordDB.selectHandle_SeedWord();
	}
	

}
