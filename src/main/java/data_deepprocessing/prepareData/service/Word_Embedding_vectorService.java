package data_deepprocessing.prepareData.service;

import java.io.BufferedReader;
import java.io.IOException;

import data_deepprocessing.prepareData.beans.Word_Embedding_VectorBean;
import data_deepprocessing.prepareData.db.Word_Embedding_vectorDB;
import data_deepprocessing.util.FilePathUtil;
import data_deepprocessing.util.FileUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午9:58:23 
* @version 1.0  
* 它的作用仅仅是给了保存运算的中间结果
*/

public class Word_Embedding_vectorService {
	
	private Word_Embedding_vectorDB word_Embedding_vectorDB;

	public void word_embedding_vector2Oracle() throws IOException{
		BufferedReader reader = FileUtil.getReader(FilePathUtil.WORD_EMBEDDING_VECTOR_PATH);
		String line ="";
		while((line = reader.readLine())!=null){
			Word_Embedding_VectorBean bean = new Word_Embedding_VectorBean(line);
			word_Embedding_vectorDB.insertVector(bean);
		}
		if(reader!=null){
			reader.close();
		}
	}
	
	
	
	
	public Word_Embedding_vectorDB getWord_Embedding_vectorDB() {
		return word_Embedding_vectorDB;
	}

	public void setWord_Embedding_vectorDB(Word_Embedding_vectorDB word_Embedding_vectorDB) {
		this.word_Embedding_vectorDB = word_Embedding_vectorDB;
	}

}















