package data_deepprocessing.prepareData.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import data_deepprocessing.algorithm.node2vec.bean.WordGraphBean;
import data_deepprocessing.prepareData.beans.Node2vec_WordBookBean;
import data_deepprocessing.prepareData.db.Node2vec_WordBookDB;
import data_deepprocessing.util.FilePathUtil;
import data_deepprocessing.util.FileUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月14日 上午8:41:16 
* @version 1.0  
*/

public class GenerateGraph2GephiService {
	private Node2vec_WordBookDB node2vec_WordBookDB;
	
	private List<Node2vec_WordBookBean> doGetWordBooks(){
		return node2vec_WordBookDB.selectAllWordBook2Word();
	}
	
	private List<Node2vec_WordBookBean> doGetPhraseBooks(){
		return node2vec_WordBookDB.selectAllWordBook2Phrase();
	}
	
	private static Map<Integer, String> num_WordMap = new HashMap<>();
	
	private static Map<Integer, String> num_PhraseMap = new HashMap<>();
	
	
	private void GenerateMap(){
		List<Node2vec_WordBookBean> wordBookBeans =  doGetWordBooks();
		List<Node2vec_WordBookBean> phraseBookBeans = doGetPhraseBooks();
		
		for(Node2vec_WordBookBean bean : wordBookBeans){
			num_WordMap.put(bean.getNum(), bean.getWord());
		}
		
		for(Node2vec_WordBookBean bean : phraseBookBeans){
			num_PhraseMap.put(bean.getNum(), bean.getWord());
		}
	}
	
	
	 
	private static List<WordGraphBean> wordGraphBeans = new ArrayList<>();
	
 	private static List<WordGraphBean> phraseGraphBeans = new ArrayList<>();
 	
 	private void GenerateList() throws IOException{
 		BufferedReader readerWord = FileUtil.getReader(FilePathUtil.node2vec_GephiPath2Word);
 		BufferedReader readerPhrase = FileUtil.getReader(FilePathUtil.node2vec_GephiPath2Phrase);
 		String line = "";
 		while((line = readerWord.readLine())!=null){
 			String[] tempStrs = line.split(" ");
 			WordGraphBean bean = new WordGraphBean(tempStrs[0],tempStrs[1],Double.valueOf(tempStrs[2]));
 			wordGraphBeans.add(bean);
 		}
 		while((line = readerPhrase.readLine())!=null){
 			String[] tempStrs = line.split(" ");
 			WordGraphBean bean = new WordGraphBean(tempStrs[0],tempStrs[1],Double.valueOf(tempStrs[2]));
 			phraseGraphBeans.add(bean);
 		}
 		
 		if(readerWord!=null){
 			readerWord.close();
 		}
 		if(readerPhrase!=null){
 			readerPhrase.close();
 		}
 	}
	
	private void transformation(){
		for(WordGraphBean bean : wordGraphBeans){
			bean.setWordOne(num_WordMap.get(Integer.valueOf(bean.getWordOne())));
			bean.setWordTwo(num_WordMap.get(Integer.valueOf(bean.getWordTwo())));
		}
		
		for(WordGraphBean bean : phraseGraphBeans){
			bean.setWordOne(num_PhraseMap.get(Integer.valueOf(bean.getWordOne())));
			bean.setWordTwo(num_PhraseMap.get(Integer.valueOf(bean.getWordTwo())));
		}
	}
	
	private void writeResulte2File() throws IOException{
		BufferedWriter writerWord = FileUtil.getWriter(FilePathUtil.node2vec_GephiPath2WordHandled);
		BufferedWriter writerPhrase = FileUtil.getWriter(FilePathUtil.node2vec_GephiPath2PhraseHandled);
		StringBuffer result = new StringBuffer();
		for(WordGraphBean bean : wordGraphBeans){
			result.append(bean.getWordOne()).append("\t").append(bean.getWordTwo())
						.append("\t").append(bean.getWeight()).append("\t").append("directed");
			writerWord.write(result.toString());
			writerWord.flush();
			writerWord.newLine();
			result.setLength(0);
		}
		for(WordGraphBean bean : phraseGraphBeans){
			result.append(bean.getWordOne()).append("\t").append(bean.getWordTwo())
			.append("\t").append(bean.getWeight()).append("\t").append("directed");
			writerPhrase.write(result.toString());
			writerPhrase.flush();
			writerPhrase.newLine();
			result.setLength(0);
		}
		if(writerWord!=null){
			writerWord.close();
		}
		if(writerPhrase!=null){
			writerPhrase.close();
		}
	}
	
	public void function(){
		GenerateMap();
		try {
			GenerateList();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		transformation();
		try {
			writeResulte2File();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	public Node2vec_WordBookDB getNode2vec_WordBookDB() {
		return node2vec_WordBookDB;
	}

	public void setNode2vec_WordBookDB(Node2vec_WordBookDB node2vec_WordBookDB) {
		this.node2vec_WordBookDB = node2vec_WordBookDB;
	}
	
	
	
}










