package data_deepprocessing.prepareData.service;

import java.util.List;

import data_deepprocessing.prepareData.beans.XianBingShi_New_Bean;
import data_deepprocessing.prepareData.beans.ZhuSu_New_Bean;
import data_deepprocessing.prepareData.db.XianBingShi_NewDB;
import data_deepprocessing.prepareData.db.ZhuSu_NewDB;
import data_deepprocessing.util.RegularExpressionUtil;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月8日 上午10:14:45 
* @version 1.0  
*/

public class HandleZhuSu_NewService {
	private ZhuSu_NewDB zhuSu_NewDB;
	private XianBingShi_NewDB xianBingShi_NewDB;
	
	
	public XianBingShi_NewDB getXianBingShi_NewDB() {
		return xianBingShi_NewDB;
	}

	public void setXianBingShi_NewDB(XianBingShi_NewDB xianBingShi_NewDB) {
		this.xianBingShi_NewDB = xianBingShi_NewDB;
	}

	public ZhuSu_NewDB getZhuSu_NewDB() {
		return zhuSu_NewDB;
	}

	public void setZhuSu_NewDB(ZhuSu_NewDB zhuSu_NewDB) {
		this.zhuSu_NewDB = zhuSu_NewDB;
	}
	
	public List<ZhuSu_New_Bean> getZhuSu_NewList(){
		return zhuSu_NewDB.selectAllZhuSu();
	}



	public void handleZhuSu(){
		List<ZhuSu_New_Bean> beans = getZhuSu_NewList();
		for(ZhuSu_New_Bean bean : beans){
			bean.setHandled_content(RegularExpressionUtil.getRegularizedSentence(bean.getChunk_content()));
			zhuSu_NewDB.updateNewZhuSu(bean);
		}
	}
	
	public List<String> getHandledZhuSu(){
		return zhuSu_NewDB.selectHandleContent();
	}
	
	public List<XianBingShi_New_Bean> getXianBingShi_New(){
		return xianBingShi_NewDB.selectTag1XianBingShi();
	}
}













