package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.SymptomBean;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:21:48 
* @version 1.0  
*/

public interface SymptomDB {
	
	public List<SymptomBean> selectAllSymptom();
	public List<String> selectAllDistinctSymptomContent();
	public List<SymptomBean> selectByIncase_id(Integer incase_id);
	
	
	
}
