package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.SeedWord2StructureDataSetBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午9:04:48 
* @version 1.0  
*/

public interface SeedWordDB {
	
	public List<SeedWord2StructureDataSetBean> selectAllSeedWord();
	public void updateSeedWord(SeedWord2StructureDataSetBean bean);
	public List<String> selectHandle_SeedWord();

}
