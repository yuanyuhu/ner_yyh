package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.XianBingShi_New_Bean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:42:48 
* @version 1.0  
*/

public interface XianBingShi_NewDB {
	
	public List<XianBingShi_New_Bean> selectTag1XianBingShi();
	public List<XianBingShi_New_Bean> selectTag0XianBingShi();

}
