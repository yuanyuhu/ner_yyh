package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.ZhuSu_New_Bean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:42:48 
* @version 1.0  
*/

public interface ZhuSu_NewDB {
	
	public List<ZhuSu_New_Bean> selectAllZhuSu();
	
	public List<String> selectHandleContent();
	
	public void updateNewZhuSu(ZhuSu_New_Bean zhuSuBean);

}
