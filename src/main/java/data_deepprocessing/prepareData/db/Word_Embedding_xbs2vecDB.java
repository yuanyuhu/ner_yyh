package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.Word_Embedding_xbs2vecBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月5日 下午3:40:27 
* @version 1.0  
* XBS 代表 xianbignshi
*/

public interface Word_Embedding_xbs2vecDB {
	
	public List<Word_Embedding_xbs2vecBean> selectAllXBS();
	public List<String> selectSegmented();
	
	public void updateXBS(Word_Embedding_xbs2vecBean bean);

}
