package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.InitSeedBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:42:48 
* @version 1.0  
*/

public interface InitSeedDB {
	
	public List<InitSeedBean> selectAllInitSeed();
	
	public List<String>  selectAllSeedContent();
	
	//下面是对英文数据的处理，十分的重要
	
	public List<String> selectAllSeedContent2En();
	
	public List<InitSeedBean> selectAllInitSeedBean2En();
	
	public int insertXianBingShi2En(InitSeedBean xianBingShiBean);

}
