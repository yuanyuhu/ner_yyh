package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.Text_CaseBean;



/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:22:10 
* @version 1.0  
*/

public interface Text_CaseDB {
	
	public Text_CaseBean selectTextCaseByIncaseID(int incase_id);
	public List<Text_CaseBean> selectAllTxtInfo();
	public List<Text_CaseBean> selectZhuSuInfo();
	public List<Text_CaseBean> selectXianBingShiInfo();
	

}
