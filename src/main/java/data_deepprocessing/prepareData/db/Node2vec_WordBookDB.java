package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.Node2vec_WordBookBean;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:27:15 
* @version 1.0  
*/

public interface Node2vec_WordBookDB {
	
	
	public List<Node2vec_WordBookBean> selectAllWordBook2Phrase();
	
	public List<Node2vec_WordBookBean> selectAllWordBook2Word();
	
	public List<Node2vec_WordBookBean> selectAllWordBook2EN();
	
	
	public int insert2WordBook2Word(Node2vec_WordBookBean workbook);
	
	public int insert2WordBook2Phrase(Node2vec_WordBookBean workbook);
	
	
	public int insert2WordBook2EN(Node2vec_WordBookBean workbook);
	
	
	
	
	
}















