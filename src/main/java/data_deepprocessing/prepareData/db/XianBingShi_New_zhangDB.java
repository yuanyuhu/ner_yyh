package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.XianBingShi_new_zhangBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:42:48 
* @version 1.0  
*/

public interface XianBingShi_New_zhangDB {
	
	public List<XianBingShi_new_zhangBean> selectALlXianBingShi();
	public List<XianBingShi_new_zhangBean> selectTop20XianBingShi();
	public List<XianBingShi_new_zhangBean> selectTop2XianBingShi();
	public List<XianBingShi_new_zhangBean> selectTop100XianBingShi();
	
	public void update2AddContent_Segmented(XianBingShi_new_zhangBean bean);
	
	
	public List<XianBingShi_new_zhangBean> selectALlXianBingShi2En();
	public List<XianBingShi_new_zhangBean> selectTop1200XianBingShi2En();
	

}
