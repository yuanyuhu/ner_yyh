package data_deepprocessing.prepareData.db;

import data_deepprocessing.prepareData.beans.Word_Embedding_VectorBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午8:38:29 
* @version 1.0  
*/

public interface Word_Embedding_vectorDB {
	
	public void insertVector(Word_Embedding_VectorBean bean);

}
