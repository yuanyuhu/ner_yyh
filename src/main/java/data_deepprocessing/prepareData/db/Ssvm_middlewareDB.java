package data_deepprocessing.prepareData.db;


import data_deepprocessing.prepareData.beans.Ssvm_MiddlewareBean;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午8:39:05 
* @version 1.0  
*/

public interface Ssvm_middlewareDB {
	
	public void insertSsvm_Middleware(Ssvm_MiddlewareBean bean);
}
