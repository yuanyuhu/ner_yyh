package data_deepprocessing.prepareData.db;

import java.util.List;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月28日 下午12:42:48 
* @version 1.0  
*/

public interface Word_Embedding_init_seedDB {
	
	public List<String> selectInitSeed2WordEmbedding();

}
