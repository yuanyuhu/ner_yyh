package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.YYH_XianBingShiBean;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:27:15 
* @version 1.0  
*/

public interface YYH_XianBingShiDB {
	
	public List<YYH_XianBingShiBean> selectAllXianBingShi();
	public List<YYH_XianBingShiBean> selectAllXianBingShiOrderById();
	public List<YYH_XianBingShiBean> selectXianBingShi2WordEmbedding();
	
	public List<YYH_XianBingShiBean> selectXianBingShi2ShouZhen();
	public List<YYH_XianBingShiBean> selectXianBingShi2FuZhen();
	
	
	
	public int updateXianBingshiForSegment(YYH_XianBingShiBean xianbingshis);
	public int insertXianBingShi(YYH_XianBingShiBean xianBingShiBean);
	public List<String> selectXBSContent_Segmented();
	
	
	
//	public List<YYH_XianBingShiBean> findXianBingShiByCondition(List<Integer> incase_ids);
	
}















