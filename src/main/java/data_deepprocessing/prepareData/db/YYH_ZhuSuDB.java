package data_deepprocessing.prepareData.db;

import java.util.List;

import data_deepprocessing.prepareData.beans.YYH_ZhuSuBean;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:27:28 
* @version 1.0  
*/

public interface YYH_ZhuSuDB {
	
	public List<YYH_ZhuSuBean> selectAllZhuSu();
	public void insertZhuShu(YYH_ZhuSuBean zhuSuBean);

}
