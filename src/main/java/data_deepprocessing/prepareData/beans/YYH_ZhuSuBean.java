package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:27:59 
* @version 1.0  
*/

public class YYH_ZhuSuBean {
	private int id;
	private int incase_id;
	private String content;
	private String content_handled;
	
	
	public YYH_ZhuSuBean(int incase_id, String content, String content_handled) {
		super();
		this.incase_id = incase_id;
		this.content = content;
		this.content_handled = content_handled;
	}
	public YYH_ZhuSuBean(){}

	public int getIncase_id() {
		return incase_id;
	}

	public void setIncase_id(int incase_id) {
		this.incase_id = incase_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent_handled() {
		return content_handled;
	}

	public void setContent_handled(String content_handled) {
		this.content_handled = content_handled;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((content_handled == null) ? 0 : content_handled.hashCode());
		result = prime * result + id;
		result = prime * result + incase_id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YYH_ZhuSuBean other = (YYH_ZhuSuBean) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (content_handled == null) {
			if (other.content_handled != null)
				return false;
		} else if (!content_handled.equals(other.content_handled))
			return false;
		if (id != other.id)
			return false;
		if (incase_id != other.incase_id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "YYH_ZhuSuBean [id=" + id + ", incase_id=" + incase_id + ", content=" + content + ", content_handled="
				+ content_handled + "]";
	}

	

}

