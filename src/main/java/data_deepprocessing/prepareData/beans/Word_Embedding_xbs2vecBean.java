package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月5日 下午3:38:08 
* @version 1.0  
*/

public class Word_Embedding_xbs2vecBean {
	
	private int id;
	private String xianbingshi;
	private String segmented;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getXianbingshi() {
		return xianbingshi;
	}
	public void setXianbingshi(String xianbingshi) {
		this.xianbingshi = xianbingshi;
	}
	public String getSegmented() {
		return segmented;
	}
	public void setSegmented(String segmented) {
		this.segmented = segmented;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((segmented == null) ? 0 : segmented.hashCode());
		result = prime * result + ((xianbingshi == null) ? 0 : xianbingshi.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Word_Embedding_xbs2vecBean other = (Word_Embedding_xbs2vecBean) obj;
		if (id != other.id)
			return false;
		if (segmented == null) {
			if (other.segmented != null)
				return false;
		} else if (!segmented.equals(other.segmented))
			return false;
		if (xianbingshi == null) {
			if (other.xianbingshi != null)
				return false;
		} else if (!xianbingshi.equals(other.xianbingshi))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Word_embedding_xbs2vecBean [id=" + id + ", xianbingshi=" + xianbingshi + ", segmented=" + segmented
				+ "]";
	}
	
	

}

















