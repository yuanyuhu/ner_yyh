package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:23:04 
* @version 1.0  
*/

public class SymptomBean {
	private int incase_id;
	private String clinical_feature;
	
	
	public int getIncase_id() {
		return incase_id;
	}
	public void setIncase_id(int incase_id) {
		this.incase_id = incase_id;
	}
	public String getClinical_feature() {
		return clinical_feature;
	}
	public void setClinical_feature(String clinical_feature) {
		this.clinical_feature = clinical_feature;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clinical_feature == null) ? 0 : clinical_feature.hashCode());
		result = prime * result + incase_id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymptomBean other = (SymptomBean) obj;
		if (clinical_feature == null) {
			if (other.clinical_feature != null)
				return false;
		} else if (!clinical_feature.equals(other.clinical_feature))
			return false;
		if (incase_id != other.incase_id)
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "SymptomBean [incase_id=" + incase_id + ", clinical_feature=" + clinical_feature + "]";
	}
	
}
