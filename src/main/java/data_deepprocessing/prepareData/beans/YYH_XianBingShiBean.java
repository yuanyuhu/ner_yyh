package data_deepprocessing.prepareData.beans;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:24:40 
* @version 1.0  
*/

public class YYH_XianBingShiBean {
	
	private int id;
	private int incase_id;
	private String content;
	private String content_handled;
	private String contain_zhusu;   //主要是方便，初诊病历做训练集，复诊病历做测试集。
	private String sign;			//由于现病史的构成由三部分构成，现病史，四诊情况，舌脉诊，方便后面的结合
	private String content_segmented;
	
	public YYH_XianBingShiBean(){}
	
	
	
	public YYH_XianBingShiBean(int incase_id, String content, String content_handled, String contain_zhusu,
			String sign, String content_segmented) {
		super();
		this.incase_id = incase_id;
		this.content = content;
		this.content_handled = content_handled;
		this.contain_zhusu = contain_zhusu;
		this.sign = sign;
		this.content_segmented = content_segmented;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIncase_id() {
		return incase_id;
	}
	public void setIncase_id(int incase_id) {
		this.incase_id = incase_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContent_handled() {
		return content_handled;
	}
	public void setContent_handled(String content_handled) {
		this.content_handled = content_handled;
	}
	public String getContain_zhusu() {
		return contain_zhusu;
	}
	public void setContain_zhusu(String contain_zhusu) {
		this.contain_zhusu = contain_zhusu;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getContent_segmented() {
		return content_segmented;
	}
	public void setContent_segmented(String content_segmented) {
		this.content_segmented = content_segmented;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contain_zhusu == null) ? 0 : contain_zhusu.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((content_handled == null) ? 0 : content_handled.hashCode());
		result = prime * result + ((content_segmented == null) ? 0 : content_segmented.hashCode());
		result = prime * result + id;
		result = prime * result + incase_id;
		result = prime * result + ((sign == null) ? 0 : sign.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YYH_XianBingShiBean other = (YYH_XianBingShiBean) obj;
		if (contain_zhusu == null) {
			if (other.contain_zhusu != null)
				return false;
		} else if (!contain_zhusu.equals(other.contain_zhusu))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (content_handled == null) {
			if (other.content_handled != null)
				return false;
		} else if (!content_handled.equals(other.content_handled))
			return false;
		if (content_segmented == null) {
			if (other.content_segmented != null)
				return false;
		} else if (!content_segmented.equals(other.content_segmented))
			return false;
		if (id != other.id)
			return false;
		if (incase_id != other.incase_id)
			return false;
		if (sign == null) {
			if (other.sign != null)
				return false;
		} else if (!sign.equals(other.sign))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "YYH_XianBingShiBean [id=" + id + ", incase_id=" + incase_id + ", content=" + content
				+ ", content_handled=" + content_handled + ", contain_zhusu=" + contain_zhusu + ", sign=" + sign
				+ ", content_segmented=" + content_segmented + "]";
	}
	
	
	
	
	

}





















