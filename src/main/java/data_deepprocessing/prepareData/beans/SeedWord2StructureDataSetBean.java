package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月7日 下午9:03:16 
* @version 1.0  
*/

public class SeedWord2StructureDataSetBean {
	
	private int id;
	
	private String seedword;
	
	private String handle_seedword;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSeedword() {
		return seedword;
	}

	public void setSeedword(String seedword) {
		this.seedword = seedword;
	}

	public String getHandle_seedword() {
		return handle_seedword;
	}

	public void setHandle_seedword(String handle_seedword) {
		this.handle_seedword = handle_seedword;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((handle_seedword == null) ? 0 : handle_seedword.hashCode());
		result = prime * result + id;
		result = prime * result + ((seedword == null) ? 0 : seedword.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeedWord2StructureDataSetBean other = (SeedWord2StructureDataSetBean) obj;
		if (handle_seedword == null) {
			if (other.handle_seedword != null)
				return false;
		} else if (!handle_seedword.equals(other.handle_seedword))
			return false;
		if (id != other.id)
			return false;
		if (seedword == null) {
			if (other.seedword != null)
				return false;
		} else if (!seedword.equals(other.seedword))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SeedWord2StructureDataSetBean [id=" + id + ", seedword=" + seedword + ", handle_seedword="
				+ handle_seedword + "]";
	}
	
	

}
