package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月9日 上午10:02:46 
* @version 1.0  
*/

public class XianBingShi_new_zhangBean {
	private int id;
	private String subxianbingshi;
	private String content_segmented;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubxianbingshi() {
		return subxianbingshi;
	}
	public void setSubxianbingshi(String subxianbingshi) {
		this.subxianbingshi = subxianbingshi;
	}
	public String getContent_segmented() {
		return content_segmented;
	}
	public void setContent_segmented(String content_segmented) {
		this.content_segmented = content_segmented;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content_segmented == null) ? 0 : content_segmented.hashCode());
		result = prime * result + id;
		result = prime * result + ((subxianbingshi == null) ? 0 : subxianbingshi.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XianBingShi_new_zhangBean other = (XianBingShi_new_zhangBean) obj;
		if (content_segmented == null) {
			if (other.content_segmented != null)
				return false;
		} else if (!content_segmented.equals(other.content_segmented))
			return false;
		if (id != other.id)
			return false;
		if (subxianbingshi == null) {
			if (other.subxianbingshi != null)
				return false;
		} else if (!subxianbingshi.equals(other.subxianbingshi))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "XianBingShi_new_zhangBean [id=" + id + ", subxianbingshi=" + subxianbingshi + ", content_segmented="
				+ content_segmented + "]";
	}
	
	
	
	

}



















