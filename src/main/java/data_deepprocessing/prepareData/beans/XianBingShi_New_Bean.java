package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月8日 上午9:58:20 
* @version 1.0  
*/

public class XianBingShi_New_Bean {
	private int yuan_id;
	private String subxianbingshi;
	
	public int getYuan_id() {
		return yuan_id;
	}
	public void setYuan_id(int yuan_id) {
		this.yuan_id = yuan_id;
	}
	public String getSubxianbingshi() {
		return subxianbingshi;
	}
	public void setSubxianbingshi(String subxianbingshi) {
		this.subxianbingshi = subxianbingshi;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subxianbingshi == null) ? 0 : subxianbingshi.hashCode());
		result = prime * result + yuan_id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XianBingShi_New_Bean other = (XianBingShi_New_Bean) obj;
		if (subxianbingshi == null) {
			if (other.subxianbingshi != null)
				return false;
		} else if (!subxianbingshi.equals(other.subxianbingshi))
			return false;
		if (yuan_id != other.yuan_id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "XianBingShi_New_Bean [yuan_id=" + yuan_id + ", subxianbingshi=" + subxianbingshi + "]";
	}
	
	
	

}
