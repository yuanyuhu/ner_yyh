package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月8日 上午9:56:30 
* @version 1.0  
*/

public class ZhuSu_New_Bean {
	private int chunk_id;
	private String chunk_content;
	private String handled_content;
	
	public int getChunk_id() {
		return chunk_id;
	}
	public void setChunk_id(int chunk_id) {
		this.chunk_id = chunk_id;
	}
	public String getChunk_content() {
		return chunk_content;
	}
	public void setChunk_content(String chunk_content) {
		this.chunk_content = chunk_content;
	}
	public String getHandled_content() {
		return handled_content;
	}
	public void setHandled_content(String handled_content) {
		this.handled_content = handled_content;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chunk_content == null) ? 0 : chunk_content.hashCode());
		result = prime * result + chunk_id;
		result = prime * result + ((handled_content == null) ? 0 : handled_content.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZhuSu_New_Bean other = (ZhuSu_New_Bean) obj;
		if (chunk_content == null) {
			if (other.chunk_content != null)
				return false;
		} else if (!chunk_content.equals(other.chunk_content))
			return false;
		if (chunk_id != other.chunk_id)
			return false;
		if (handled_content == null) {
			if (other.handled_content != null)
				return false;
		} else if (!handled_content.equals(other.handled_content))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ZhuSu_New_Bean [chunk_id=" + chunk_id + ", chunk_content=" + chunk_content + ", handled_content="
				+ handled_content + "]";
	}
	
	
	
	
}














