package data_deepprocessing.prepareData.beans;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年1月8日 上午10:23:24 
* @version 1.0  
*/

public class Text_CaseBean {
	
	private int incase_id;
	private String txt_info;
	
	
	public int getIncase_id() {
		return incase_id;
	}
	public void setIncase_id(int incase_id) {
		this.incase_id = incase_id;
	}
	public String getTxt_info() {
		return txt_info;
	}
	public void setTxt_info(String txt_info) {
		this.txt_info = txt_info;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + incase_id;
		result = prime * result + ((txt_info == null) ? 0 : txt_info.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Text_CaseBean other = (Text_CaseBean) obj;
		if (incase_id != other.incase_id)
			return false;
		if (txt_info == null) {
			if (other.txt_info != null)
				return false;
		} else if (!txt_info.equals(other.txt_info))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Text_CaseBean [incase_id=" + incase_id + ", txt_info=" + txt_info + "]";
	}
	

}
