package data_deepprocessing.prepareData.beans;


/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年4月12日 上午11:03:52 
* @version 1.0  
*/

public class Node2vec_WordBookBean {
	private int id;
	private String word;
	private int num;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + num;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node2vec_WordBookBean other = (Node2vec_WordBookBean) obj;
		if (id != other.id)
			return false;
		if (num != other.num)
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Node2vec_WorkBookBean [id=" + id + ", word=" + word + ", num=" + num + "]";
	}
	

}
