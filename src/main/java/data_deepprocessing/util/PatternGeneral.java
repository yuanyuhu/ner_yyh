package data_deepprocessing.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class PatternGeneral {

	/** 
	 * @author yyh
	 * 
	 */  
	public  static String  patternReplace(String str,String regex,String replace){  
		String s=str;
		Pattern pattern = Pattern.compile(regex);  
		Matcher matcher = pattern.matcher(str); 
		s = matcher.replaceAll(replace); 
		return s;
	}

}
