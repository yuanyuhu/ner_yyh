package data_deepprocessing.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


import com.sun.jna.Library;
import com.sun.jna.Native;


/**
 * 对中文分词工具的调用，这里就给它设计成工具类的形式就好了，剩下的内容就交给程序自己做吧
 * @author Administrator
 *
 */
public class NlpirSegmentUtil {
	
	
	public static Logger logger = Logger.getLogger("NlpirSegmentUtil");
	
	
	public interface CLibrary extends Library {
		String path = System.getProperty("user.dir");
		CLibrary Instance = (CLibrary) Native.loadLibrary(path+File.separator+"NLPIRLib"+File.separator+"NLPIR", CLibrary.class);
		public int NLPIR_Init(String sDataPath, int encoding,String sLicenceCode);
		public String NLPIR_ParagraphProcess(String sSrc, int bPOSTagged);
		public String NLPIR_GetKeyWords(String sLine, int nMaxKeyLimit,boolean bWeightOut);
		public String NLPIR_GetFileKeyWords(String sLine, int nMaxKeyLimit,boolean bWeightOut);
		public int NLPIR_AddUserWord(String sWord);//add by qp 2008.11.10
		public int NLPIR_DelUsrWord(String sWord);//add by qp 2008.11.10
		public String NLPIR_GetLastErrorMsg();
		public void NLPIR_Exit();
	}
	
	/** 
	* @author  作者 : YUHU YUAN
	* @date 创建时间：2017年3月9日 上午9:11:16 
	* @parameter 
	* @return
	* @throws
	* 改变字符串的编码方式
	*/
	public static String transString(String aidString, String ori_encoding,String new_encoding) {
		try {
			return new String(aidString.getBytes(ori_encoding), new_encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public void test(String sentenceContent,List<String> symptoms){
		String argu = System.getProperty("user.dir");
		int charset_type = 1;
		int init_flag = CLibrary.Instance.NLPIR_Init(argu, charset_type, "0");
		String nativeBytes = null;
		if (0 == init_flag) {
			nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			logger.info("初始化失败！fail reason is "+nativeBytes);
		}
		try {
			for(String symptom_Name : symptoms){
				CLibrary.Instance.NLPIR_AddUserWord(symptom_Name);
			}
			String sInput = sentenceContent;
			nativeBytes = CLibrary.Instance.NLPIR_ParagraphProcess(sInput, 1);
			Logger.getLogger("yuan_data").info("增加用户词典后分词结果为:" + nativeBytes);
			
			CLibrary.Instance.NLPIR_Exit();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
	}

	public static void main(String[] args){
		NlpirSegmentUtil nlpirSegmentForWordCluster = new NlpirSegmentUtil();
		List<String> list = new ArrayList<>();
		list.add("口干");
		list.add("舌燥");
		nlpirSegmentForWordCluster.test("间断性口干舌燥三天。", list);
	}
	
	
}



