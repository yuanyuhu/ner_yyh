package data_deepprocessing.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TxtOperate {

	public static boolean delAllFile(String path) {
		boolean  flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);//��ɾ���ļ���������ļ�

				flag = true;
			}
		}
		return flag;
	}
	//�жϴ���·���е��ļ��Ƿ���� �����ھʹ���  
	public static File newTxt(String folderPath,String fileName) { 	
		File   f=   new   File(folderPath+"\\"+fileName+".txt");     	
		try{ 
			if(!f.exists()){
				f.createNewFile();
			} 
		}catch(IOException   e){ 
			System.out.println(e); 
		}  
		System.out.println("create "+fileName);
		return f;  			
	}
	//д�ļ�
//	public static void writeTxtFile(String content,File  file,boolean flagadd)throws Exception{
//		FileOutputStream fos = new FileOutputStream(file,flagadd);            
//		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,"GBK") );         
//		bw.write(content);
//		if(bw!=null){
//			bw.close();
//		}
//	} 
	public static void writeTxtFile(String content,File  file,boolean flagadd)throws Exception{
		FileOutputStream fos = new FileOutputStream(file,flagadd);            
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,"UTF-8") );         
		bw.write(content);
		bw.flush();
		if(bw!=null){
			bw.close();
		}
	} 
	//���ļ�
	public static String readTxtFile(String filePath,String encoding){
		String content="";
		try {
			//String encoding="GBK";
			File file=new File(filePath);
			if(file.isFile() && file.exists()){ //�ж��ļ��Ƿ����
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file),encoding);//���ǵ������ʽ
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while((lineTxt = bufferedReader.readLine()) != null){
					content=content+lineTxt+"\r\n";
				}
				read.close();
			}else{
				System.out.println("�Ҳ���ָ�����ļ�");
			}
		} catch (Exception e) {
			System.out.println("��ȡ�ļ����ݳ���");
			e.printStackTrace();
		}
		return content;
	}
	/**
	 * ���ļ���ָ�����ݵĵ�һ���滻Ϊ��������.
	 * 
	 * @param oldStr
	 *            ��������
	 * @param replaceStr
	 *            �滻����
	 * @throws IOException 
	 */
	public static void replaceTxtByStr(File file,String oldStr,String replaceStr,int start ,String tagName) throws IOException {

		String temp = "";
		int linNum=0;
		int m=0;
		try {
			//File file = new File(f);
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis,"UTF-8");
			BufferedReader br = new BufferedReader(isr);
			StringBuffer buf = new StringBuffer();

			// �������ǰ��������ҵ�Ҫ�滻�����ֵ����ݣ�������ǰ������ݱ���
			for (int j = 0; (temp = br.readLine()) != null&&j<start; j++) {
				buf = buf.append(temp);
				buf = buf.append(System.getProperty("line.separator"));

				linNum=j;
				m=j;
			}
			String str="";
			if(tagName.contains("֢״")){
				str="S";
			}else if(tagName.contains("����")){
				str="D";
			}else if(tagName.contains("����")){
				str="I";
			}else if(tagName.contains("ʱ����")){
				str="T";
			}else{
				str="O";
			}

			for(int k=0;k<replaceStr.length();k++){
				if(k==0){
					buf = buf.append(replaceStr.substring(k,k+1)+"\tB-"+str);
					buf = buf.append(System.getProperty("line.separator"));
				}else{
					buf = buf.append(replaceStr.substring(k,k+1)+"\tE-"+str);
					buf = buf.append(System.getProperty("line.separator"));							
				}
			}

			linNum=linNum+replaceStr.length()-1;
			while((temp = br.readLine())!=null){
				if(m<linNum){
					m++;
					continue;
				}else{
					buf = buf.append(temp);
					buf = buf.append(System.getProperty("line.separator"));

				}
			}

			br.close();
			FileOutputStream fos = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(fos,"UTF-8"));
			pw.write(buf.toString().toCharArray());
			pw.flush();
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// �ļ����ݵ���������  
	public static int getTotalLines(File file) throws IOException {  
		FileReader in = new FileReader(file);  
		LineNumberReader reader = new LineNumberReader(in);  
		String s = reader.readLine();  
		int lines = 0;  
		while (s != null) {  
			lines++;  
			s = reader.readLine();  
		}  
		reader.close();  
		in.close();  
		return lines;  
	}  
	public static void createFile(String filePath){
		File file =new File(filePath);    
		//����ļ��в������򴴽�    
		if  (!file .exists()  && !file .isDirectory())      
		{       
			System.out.println("//������");  
			file .mkdir();   
		} else   
		{  
			System.out.println("//Ŀ¼����");  
		}  
	}
	/**
	 * ��ȡĳ���ļ����µ������ļ�
	 */
	public static boolean readfile(String filepath) throws FileNotFoundException, IOException {
		List<String> fileNameList=new ArrayList<String>();

		File file = new File(filepath);
		if (!file.isDirectory()) {
			System.out.println("�ļ�");
			System.out.println("path=" + file.getPath());
			System.out.println("absolutepath=" + file.getAbsolutePath());
			System.out.println("name=" + file.getName());
			fileNameList.add(file.getName());
		} 
		return true;
	}
	//��ȡ������ʽ
	public static String readRegex(String filePath) {
		String regex="";
		File file=new File(filePath);
		try {
			InputStreamReader read = new InputStreamReader(new FileInputStream(file),"UTF-8");//�㻹��̫�����ˣ��Ǻ�
			BufferedReader bufferedReader = new BufferedReader(read);
			int k=	TxtOperate.getTotalLines(file);
			String lineTxt = null;
			int i=0;
			while((lineTxt = bufferedReader.readLine()) != null){
				if(i!=k-1){
					regex=regex+"("+lineTxt+")"+"|";
				}else{
					regex=regex+lineTxt;
				}
				i++;
			}
			read.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return regex;
	}
	//public static void main(String[] args) throws IOException{
	//	String filePath="E:\\ess\\20140215\\�½��ļ��� (2)\\catcm.ess.main\\data/emr/train";	
	//	File   f=   new   File(filePath+"\\"+"2014-02-187337.txt");
	//	String str="����10��,���ȡ�����2��";
	//	String replaceStr="����";
	//	String replaceStr1="����";
	//	int start1=str.indexOf(replaceStr1);
	//    int start=str.indexOf(replaceStr);	
	//    System.out.println(start);
	//	replaceTxtByStr(f,replaceStr,replaceStr,start);
	//	replaceTxtByStr(f,replaceStr1,replaceStr1,start1);
	//}
}
