package data_deepprocessing.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;


/**
 * @author YuanYuhu
 */
public class FileUtils {	   
	/**获取指定文件夹下的所有文件
	 * */
	public   Map<Integer,File> getFile(String path){    
		Map<Integer,File> map=new HashMap<Integer,File> ();
		File file = new File(path);    
		//System.out.println(path+"dddd");
		File[] array = file.listFiles();    

		for(int i=0;i<array.length;i++){    
			if(array[i].isFile()){    
				map.put(i, array[i]);
			} 
		} 
		return map;
	}
	//移动文件
	public  void moveFolder(String src, String dest) { 
		File srcFolder = new File(src); 
		File destFolder = new File(dest);  
		File newFile = new File(destFolder.getAbsoluteFile() + "\\" + srcFolder.getName()); 
		srcFolder.renameTo(newFile); 
	}
	//删除指定文件
	public void delFile(String filePath){
		File file = new File(filePath);
		if(file.exists()){
			boolean d = file.delete();
			if(d){
				System.out.print("删除成功！");
			}else{
				System.out.print("删除失败！");
			}
		}
	}
	//删除指定文件夹下所有文件
	//param path 文件夹完整绝对路径
	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);//先删除文件夹里面的文件
				//  delFolder(path + "/" + tempList[i]);//再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}
	// 复制文件
    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            // 新建文件输入流并对它进行缓冲
            inBuff = new BufferedInputStream(new FileInputStream(sourceFile));
            // 新建文件输出流并对它进行缓冲
            outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));
            // 缓冲数组
            byte[] b = new byte[1024 * 5];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            // 刷新此缓冲的输出流
            outBuff.flush();
        } finally {
            // 关闭流
            if (inBuff != null)
                inBuff.close();
            if (outBuff != null)
                outBuff.close();
        }
    }
    /**
     * 
     * @param srcFileName
     * @param destFileName
     * @param srcCoding
     * @param destCoding
     * @throws IOException
     */
    public static void copyFile(File srcFileName, File destFileName, String srcCoding, String destCoding) throws IOException {// 把文件转换为GBK文件
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(srcFileName), srcCoding));
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destFileName), destCoding));
            char[] cbuf = new char[1024 * 5];
            int len = cbuf.length;
            int off = 0;
            int ret = 0;
            while ((ret = br.read(cbuf, off, len)) > 0) {
                off += ret;
                len -= ret;
            }
            bw.write(cbuf, 0, off);
            bw.flush();
        } finally {
            if (br != null)
                br.close();
            if (bw != null)
                bw.close();
        }
    }
	//创建文件夹
	public File doCreateFile(String filePath,String fileName) throws IOException{
		File outfile    = new File(filePath+"/"+fileName);                 
		  //如果文件不存在，则创建一个新文件
		  if(!outfile.isFile()){
		      outfile.mkdir(); ;
		      return outfile;
		}
		  return null;
	}
}
