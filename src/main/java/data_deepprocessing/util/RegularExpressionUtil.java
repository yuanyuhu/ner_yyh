package data_deepprocessing.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegularExpressionUtil {
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String string = "2434年4月4日我发发3445年发斯蒂芬342年3月大师傅，5月3日阿斯顿发放啊方，于686-534-34日我们3片/日。";
		System.out.println(string);
		string = RegularExpressionUtil.getRegularizedSentence(string);
		System.out.println(string);
	}
	/**
	 * 对句子进行规范化处理，时间，标点和单位
	 * @param sentence
	 * @return
	 */
	public static String getRegularizedSentence(String sentence){
		
		String ProjectPath=System.getProperty("user.dir");
		
		String timePath=ProjectPath+"/regex/timeregex.txt";
		
		String dosePath=ProjectPath+"/regex/doseregex.txt";
		
		String regexTime=TxtOperate.readRegex(timePath);
		
		String regexdose=TxtOperate.readRegex(dosePath);
		
		sentence = PatternGeneral.patternReplace(sentence, regexTime, "时间");
		
		sentence = PatternGeneral.patternReplace(sentence, regexdose, "单位");
		
		sentence = sentence.replaceAll("[\\p{P}+~$`^=|<>～｀＄＾＋＝｜＜＞￥×℃ⅠⅡμ]+", "符号");
		
		sentence = sentence.replaceAll("时间", "TU");
		
		sentence = sentence.replaceAll("单位", "DU");
		
		sentence = sentence.replaceAll("符号", "S");
		
		return sentence;
	}
	
	public static String HandlePunctuation2EnSentence(String sentence){
		sentence = sentence.replaceAll("[\\p{P}+~$`^=|<>～｀＄＾＋＝｜＜＞￥×℃ⅠⅡμ]+", "");
		return sentence;
	}
	
	
	/**
	 * 现病史用#号隔开后用的正则表达式把它给提取出来
	 * 这里主要是处理张老师的数据，把张老师的数据从段落中拿出来
	 * @param sentence
	 * @return
	 */
	public static String extractXianbingshi(String sentence){
		String xianbingshi = null;
		
		String regex = "(?<=#现病史).+?(?=#)";
		
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(sentence);
		
		while(matcher.find()){
			xianbingshi = matcher.group();
		}
		
		return xianbingshi;
		
	}
	
	


}
