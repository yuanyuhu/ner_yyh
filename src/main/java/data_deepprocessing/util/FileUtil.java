package data_deepprocessing.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author YuanYuhu
 */
public class FileUtil {
	/**
	 * 获取指定文件夹下的所有文件
	 * @param path
	 * @return
	 */
	public  static List<File> getFile(String SinglePath){  
		List<File> files = new ArrayList<File>();
		File file = new File(SinglePath);    
		File[] array = file.listFiles();
		for(int i=0;i<array.length;i++){    
			files.add(array[i]);
		} 
		return files;
	}
	
	
	public static FileOutputStream writeFile(String filename) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return out;
	}
	
	
	public static BufferedWriter getWriter(String outpath){
		FileOutputStream fos = null;
		BufferedWriter writer = null;
		try{
			fos = new FileOutputStream(new File(outpath),true);      
			writer = new BufferedWriter(new OutputStreamWriter(fos,"utf-8") );
		}catch(IOException e){
			e.printStackTrace();
			try {
				writer.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return writer;
		
	}
	
	private static String getCharset(File fileName) throws IOException{  
        
        BufferedInputStream bin = new BufferedInputStream(new FileInputStream(fileName));    
        int p = (bin.read() << 8) + bin.read();    
          
        String code = null;    
          
        switch (p) {    
            case 0xefbb:    
                code = "UTF-8";    
                break;    
            case 0xfffe:    
                code = "Unicode";    
                break;    
            case 0xfeff:    
                code = "UTF-16BE";    
                break;    
            default:    
                code = "GBK";    
        }    
        return code;  
	}  
	
	public static BufferedReader getReader(String inpath){
		InputStreamReader read = null;
		BufferedReader reader = null;
		try{
			read = new InputStreamReader(new FileInputStream(new File(inpath)),"utf-8");
			reader = new BufferedReader(read);
		}catch(IOException e){
			try {
				reader.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return reader;
	}
	
	public static BufferedReader getReader(File file){
		InputStreamReader read = null;
		BufferedReader reader = null;
		try{
			read = new InputStreamReader(new FileInputStream(file),"utf-8");
			reader = new BufferedReader(read);
		}catch(IOException e){
			try {
				reader.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return reader;
	}
}





