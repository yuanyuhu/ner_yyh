package data_deepprocessing.util;

import java.io.File;

/** 
* @author  作者 : YUHU YUAN
* @date 创建时间：2017年3月31日 下午10:03:31 
* @version 1.0  
* 1：专门用来得到各种路径；
* 2：也可以用专门的配置文件来搞定；
* 注意：
* 这里面SSVM_FORMAT_FILE_PATH  和 SVM_INPUT 是同一个文件
* 这样写主要是程序中好看，形成的format文件就是给svm用的
*/

public class FilePathUtil {
	
	public final static String FILEPATH =System.getProperty("user.dir")+File.separator+"temp"+File.separator;
	public final static String WORD_EMBEDDING_VECTOR_PATH =FILEPATH+"word_embedding_vector.txt";
	public final static String WORD_EMBEDDING_MODEL_PATH =FILEPATH+"word_embedding_model.txt";
	public final static String SEGMENTED_XIANBINGSHI_PATH =FILEPATH+"segmented_xianbingshi.txt";
	public final static String SSVM_FORMAT_FILE_PATH =FILEPATH+"ssvm_format_file.dat";
	public final static String SSVM_FORMAT_FILE_JUDGE_PATH =FILEPATH+"ssvm_format_jugde_file.txt";
	
	public final static String SSVM_MODEL =FILEPATH+ "ssvm_model.txt";
	public final static String SSVM_INPUT =FILEPATH+ "ssvm_format_file.dat";
	public final static String SSVM_OUTPUT =FILEPATH+ "ssvm_output.txt";
	
	
	public final static String SVMFILEPATH =System.getProperty("user.dir")+File.separator+"ssvm_exe"+File.separator;
	public final static String svm_hmm_learn =SVMFILEPATH+ "svm_hmm_learn";
	public final static String svm_hmm_classify =SVMFILEPATH+ "svm_hmm_classify";
	
	
	
	
	public final static String node2vec_vectorPath =FILEPATH+ "node2vec_result2phrase.txt";
	public final static String node2vec_vectorPath2Word =FILEPATH+ "node2vec_result2word.txt";
	
	//这里的问题是要生成的给node2vec程序用的&也要用来给Gephi展示，所以也非常的重要
	public final static String node2vec_GephiPath2Word =FILEPATH+ "node2vec_word_edgelist.txt";
	public final static String node2vec_GephiPath2Phrase =FILEPATH+ "node2vec_phrase_edgelist.txt";
	//把数字改成字或者词
	public final static String node2vec_GephiPath2WordHandled =FILEPATH+ "node2vec_word_edgelist2word.txt";
	public final static String node2vec_GephiPath2PhraseHandled =FILEPATH+ "node2vec_phrase_edgelist2phrase.txt";
	
	
	//对英文数据的处理
	public final static String SEGMENTED_XIANBINGSHI_EN_PATH = FILEPATH+"segmented_xianbingshi_EN.txt";
	public final static String WORD_EMBEDDING_VECTOR_EN__PATH = FILEPATH+"word_embedding_vector_EN.txt";
	public final static String node2vec_GephiPath2EN =FILEPATH+ "node2vec_EN_edgelist.txt";
	public final static String node2vec_vectorPath2EN =FILEPATH+ "node2vec_result2EN.txt";
	
	
}















