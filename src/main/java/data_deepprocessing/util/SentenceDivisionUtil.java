package data_deepprocessing.util;

import java.util.ArrayList;
import java.util.List;



/**
 * @author YuanYuhu
 *	主要解决句子的长度分割的问题，这个问题，应该是不难
 *	这里默认的是50
 *	这个类的使用
 *SentenceDivisionUtil sentenceDicisionUtil2 = new SentenceDivisionUtil();
 *		sentenceDicisionUtil2.sentenceDivision(bean2);
 *		
 *		for(String str : sentenceDicisionUtil2.getSentences())
 *			System.out.println(bean2.getInhosptial_id()+" _ "+str+" _ "+str.length());
 *
 */
public class SentenceDivisionUtil {
	
	public List<String> sentences = new ArrayList<>();
	
	public final static int sentencemaxlengthlimit = 100;
	
	public final static int sentencelengthlimit = 50; 
	
	public final static int listelementlengthlimit = 15; 
	
	
	public void sentenceDivision(String sentence){
		
		String[] tempStrings = sentence.split("S");
		
		StringBuffer temp_sentence = new StringBuffer();
		
		for(int i=0; i<tempStrings.length; i++){
			//句子长度的约束，不能超过50
			while((i<tempStrings.length) && (temp_sentence.toString().length() < sentencelengthlimit)){
				// 因为标点符号是S来隔开的，所以就用S了，如果有改动就改这里把
				temp_sentence.append(tempStrings[i].trim()).append("S");
				++i;
			}
			
			
			if(temp_sentence.toString().length() > sentencemaxlengthlimit){
				int count = temp_sentence.toString().length()/sentencelengthlimit;
				String tempSentence = temp_sentence.toString();
				sentences.add(tempSentence.substring(0, 50));
				for(int j=1; j< count; j++){
					sentences.add(tempSentence.substring(j*50, (j+1)*50));
				}
				sentences.add(tempSentence.substring((count-1)*50, tempSentence.length()));
				
			}else if(temp_sentence.toString().length() > listelementlengthlimit){
				//判断是不是大于约束条件
				sentences.add(temp_sentence.toString());
			}else{
				if(sentences.size()>0){
					//
					StringBuffer endElement = new StringBuffer(sentences.get(sentences.size()-1));
					endElement.append(temp_sentence).append("S");
					sentences.remove(sentences.size()-1);
					sentences.add(endElement.toString());
				}else{
					sentences.add(temp_sentence.toString());
				}
			}
			
			temp_sentence.setLength(0);
		}
		
	}
	
	public static int getSentencelengthlimit() {
		return sentencelengthlimit;
	}
	
	public static int getSentencemaxlengthlimit() {
		return sentencemaxlengthlimit;
	}

	public static int getListelementlengthlimit() {
		return listelementlengthlimit;
	}


	public List<String> getSentences() {
		return sentences;
	}

}















